local item = ...
local game = item:get_game()

local amount = 1

function item:on_obtained()
  local old_damage = game:get_value("solforge_basic_sword_attack_power")
  game:set_value("solforge_basic_sword_attack_power", old_damage + amount)
end