local item = ...
local game = item:get_game()

local amount = 15

function item:on_obtained()
  game:set_max_magic(game:get_max_magic() + amount)
  game:set_magic(game:get_max_magic())
end