local item = ...
local game = item:get_game()

local gun = "sniper"
local amount = 25

function item:on_obtained()
  local previous_multiplier = game:get_value(gun .. "_ammo_cost_multiplier") or 100
  game:set_value(gun .. "_ammo_cost_multiplier", previous_multiplier - amount)
end