local item = ...
local game = item:get_game()

local amount = 2

function item:on_obtained()
  game:add_max_life(amount)
  game:set_life(game:get_max_life())
end