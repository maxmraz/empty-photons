local item = ...
local game = item:get_game()

function item:on_obtaining()
  game:set_value("ability_can_deflect_projectiles", true)
end