local item = ...

local step_forward = function()
  local hero = sol.main.get_game():get_hero()
  local m = sol.movement.create("straight")
  local direction = hero:get_direction()
  m:set_angle(direction * math.pi / 2)
  m:set_max_distance(4)
  m:start(hero)
end

require("items/solforge/lib/forge"):temper_weapon(item,
  {
    item_id = item:get_name():gsub("/", "_"),

    attacks = {
      {
        windup_duration = 300,
        hero_windup_animation = "sword_windup",
        hero_attack_animation = "sword_swing",
        weapon_sprite = "solforge_weapons/claymore",
        weapon_windup_animation = "windup",
        weapon__attack_animation = "sword",
        weapon_sound = "sword1",
        callback = step_forward,
      },
      {
        windup_duration = 200,
        hero_windup_animation = "sword_windup_reverse",
        hero_attack_animation = "sword_swing_backhand",
        weapon_sprite = "solforge_weapons/claymore",
        weapon_windup_animation = "windup_reverse",
        weapon__attack_animation = "reverse_swing",
        weapon_sound = "sword1",
        callback = step_forward,
      },
      {
        windup_duration = 400,
        hero_windup_animation = "sword_windup",
        hero_attack_animation = "sword_swing",
        weapon_sprite = "solforge_weapons/claymore",
        weapon_windup_animation = "windup",
        weapon__attack_animation = "endcombo",
        weapon_sound = "sword1",
        attack_power_bonus = 1,
      },
    },


    weapon_parameters = {
      attack_power = 4,
    },

  }
)
