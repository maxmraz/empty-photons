local item = ...

local step_forward = function()
  local step = 16
  local game = sol.main.get_game()
  local map = game:get_map()
  local hero = game:get_hero()
  local direction = hero:get_direction()
  local dx = game:dx(step)[direction]
  local dy = game:dy(step)[direction]
  local x, y, z = hero:get_position()
  local test_x = x + dx
  local test_y = y + dy
  local test_ground = map:get_ground(test_x, test_y, z)
  local are_obstacles = hero:test_obstacles(dx, dy) or (test_ground == "deep_water") or (test_ground == "hole") or (test_ground == "lava")
  if not are_obstacles then
    local m = sol.movement.create("straight")
    m:set_angle(direction * math.pi / 2)
    m:set_max_distance(step)
    m:set_speed(90)
    m:start(hero)
  end
end

local swing_sounds = {"sword1", "sword2", "sword3", "sword4"}

item.light_source_sprite = "solforge_weapons/sword_1_glow"

require("items/solforge/lib/forge"):temper_weapon(item,
  {
    item_id = item:get_name():gsub("/", "_"),

    attacks = {
      {
        windup_duration = 100,
        hero_windup_animation = "sword_windup",
        hero_attack_animation = "sword_swing",
        weapon_sprite = "solforge_weapons/sword_1",
        weapon_windup_animation = "swing_windup",
        weapon__attack_animation = "swing",
        weapon_sound = swing_sounds,
        callback = step_forward,
      },
      {
        hero_attack_animation = "sword_swing_backhand",
        weapon_sprite = "solforge_weapons/sword_1",
        weapon__attack_animation = "reverse_swing",
        weapon_sound = swing_sounds,
        callback = step_forward,
      },
      {
        windup_duration = 80,
        hero_windup_animation = "sword_windup",
        hero_attack_animation = "sword_swing",
        weapon_sprite = "solforge_weapons/sword_1",
        weapon_windup_animation = "swing_windup",
        weapon__attack_animation = "swing",
        weapon_sound = swing_sounds,
        callback = step_forward,
        attack_power_bonus = 1,
      },
    },


    weapon_parameters = {
      attack_power = 2,
    },

  }
)
