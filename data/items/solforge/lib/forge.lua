--Created by Max Mraz and J. Cournoyer, licensed with the MIT license
--See example weapons for examples.
--Minimum viable weapon is:
--local item = ...
--require("items/solforge/forge"):new_weapon(item)
--That will give you an item with all the default values

--Default behavior is defined in attack_collision_callback.lua, feel free to overwrite that to suit your game

local weapon_factory = {}
local collision_callback_manager = require("items/solforge/lib/attack_collision_callback")
local weapon_use_manager = require"items/solforge/lib/weapon_use_callback"

local post_attack_combo_window = 400 --how long after an attack before the next move won't be part of a combo

--Applies solforge properties to the item supplied, based on the properties supplied:
function weapon_factory:temper_weapon(item, props)

  local parameters =  props.weapon_parameters

  function item:on_started()
    item.item_id = props.item_id or item:get_name():gsub("/", "_")
    local savegame_variable = "possession_sf_" .. item.item_id
    item:set_savegame_variable(savegame_variable)

    item:set_assignable(true)

    item:set_properties(props)
  end

  function item:set_properties(props)
    local game = item:get_game()
    if props == nil then props = {} end

    --item.callback is a function that is called whenever using the item
    item.callback = props.callback or nil

    --Add a parameter to determine the draw order. True by default if not defined in properties table.
    item.drawn_in_y_order = props.drawn_in_y_order or true

    --item.attacks is a table of attacks, each of which is a table specifying hero and weapon animations, sound, and damage
    --attack_power_bonus is optional, its an amount to add to the weapon's base attack power for this attack
    --It is only processed in attack_collision_callback, so can be altered there however you want
    --You can also define any number of values for an attack, and use them in attack_collision_callback however you want
    --multiple attacks in item.attacks means a combo
    item.attacks = props.attacks or {
      {
        hero_attack_animation = "sword_swing",
        weapon_sprite = "hero/sword1",
        weapon__attack_animation = "sword",
        weapon_sound = "sword1",
        attack_power_bonus = nil,
      }
    }

    --Save weapon parameters as savegame values, so you can modify them if needed
    for k,v in pairs(parameters or {}) do
      if game:get_value(item.item_id .. "_" .. k) == nil then
        item.k = v
        assert(type(v) == "number" or "string" or "bool", "Item value of key: " .. k .. " is not a number, string, or boolean value.")
        game:set_value(item.item_id .. "_" .. k, v )
      end
    end

  end


  function item:on_using()
    local map = item:get_map()
    local hero = map:get_hero()
    local game = map:get_game()
    local combo_number = hero.combo_number or 1
    local x, y, z = hero:get_position()
    local direction = hero:get_direction()
    if hero.weapon_entity then hero.weapon_entity:remove() end

    --Check if the weapon needs a resource (magic, stamina, etc.)
    local can_use = item:check_use_cost()

    --Check the durability of the weapon before attacking.
    local broken = game:get_value(item.item_id .. "_durability") == (0 or nil)

    if broken or not can_use then
      item:set_finished()
      return
    end

    --Create the weapon as a custom entity:
    hero.weapon_entity = map:create_custom_entity{
      x = x, y = y, layer = z,
      direction = direction, width = 16, height = 16,
      sprite = item.attacks[combo_number].weapon_sprite,
    }
    --Set the weapon draw order based on the item.drawn_in_y_order value.
    hero.weapon_entity:set_drawn_in_y_order(item.drawn_in_y_order)
    --Cybersea addition: add weapon light source sprite
    if item.light_source_sprite then
      hero.weapon_entity.light_source_sprite = item.light_source_sprite
      hero.weapon_entity.light_source_sprite_animation_matched = true
    end

    --Put player into custom attack state while attacking
    hero:start_state(item:get_attack_state())

    --Set hero and weapon in windup animations:
    local attack = item.attacks[combo_number]
    attack.windup_duration = attack.windup_duration or 0
    if attack.windup_duration > 0 then
      hero:set_animation(attack.hero_windup_animation)
      if attack.windup_sound then sol.audio.play_sound(attack.windup_sound) end
      hero.weapon_entity:get_sprite():set_animation(attack.weapon_windup_animation)
      sol.timer.start(hero, attack.windup_duration, function()
        item:start_attack()
      end)
    else
      item:start_attack()
    end
  end



  function item:start_attack(attack)
    local map = item:get_map()
    local hero = map:get_hero()
    local game = map:get_game()
    local combo_number = hero.combo_number or 1
    attack = attack or item.attacks[combo_number]
    local x, y, z = hero:get_position()
    local direction = hero:get_direction()
    hero.weapon_entity:get_sprite():set_animation(attack.weapon__attack_animation, function()
      hero.weapon_entity:remove()
      hero.weapon_entity = nil
    end)

    weapon_use_manager:initiate_usage(item)
    hero.weapon_entity.collided_entities = {}

    --What happens when the attack hits stuff is defined in attack_collision_callback.lua
    hero.weapon_entity:add_collision_test("sprite", function(weapon_entity, other_entity, sprite, other_sprite)
      --Dedup check
      if hero.weapon_entity.collided_entities[other_entity] then return end
      hero.weapon_entity.collided_entities[other_entity] = true

      collision_callback_manager:process_collision({
        weapon_entity = weapon_entity,
        other_entity = other_entity,
        weapon_sprite = sprite,
        other_sprite = other_sprite,
        item = item,
        attack = attack,
        game = game,
        map = map,
        hero = hero,
      })
    end)

    if type(attack.weapon_sound) == "table" then
      local rand = math.random(1, #attack.weapon_sound)
      sol.audio.play_sound(attack.weapon_sound[rand])
    else
      sol.audio.play_sound(attack.weapon_sound or "sword1")
    end


    hero:set_animation(attack.hero_attack_animation, function()
      if attack.recovery_duration then
        hero:set_animation(attack.hero_recovery_animation)
        sol.timer.start(hero, attack.recovery_duration, function()
          hero:unfreeze()
          item:set_finished() 
        end)
      else
        hero:unfreeze()
        item:set_finished() 
      end
    end)

    --Call item and attack callbacks, if any
    if item.callback then item.callback() end
    if attack.callback then attack.callback() end

    --Manage attack combo iteration:
    if item.combo_timer then item.combo_timer:stop() end --remove previous timer
    hero.combo_number = combo_number + 1
    if hero.combo_number > #item.attacks then hero.combo_number = 1 end --return to attack 1 if max is reached
    --Set the window for the next attack to 200ms after the animation finishes
    local timer_length = attack.windup_duration + hero.weapon_entity:get_sprite():get_num_frames() * hero.weapon_entity:get_sprite():get_frame_delay() + post_attack_combo_window
    item.combo_timer = sol.timer.start(map, timer_length, function()
      hero.combo_number = 1
    end)
  end


  --Check if the hero has enough of any resource to use the weapon, and remove the cost, if applicable
  function item:check_use_cost()
    local map = item:get_map()
    local game = map:get_game()
    local weapon = item.item_id
    local enough_resource = true

    if game:get_value(weapon .. "_cost_type") == nil then return enough_resource end
    if game:get_value(weapon.."cost_amount") == nil then return enough_resource end

    local current_resource_amount
    --TODO???: Create game_meta function to get the appropriate resource type.
    --Could set up game_meta functions for the getting and setting of the resource.
    if game:get_value(weapon .. "_cost_type") == "magic" then
      current_resource_amount = game:get_magic()
    elseif game:get_value(weapon .. "_cost_type") == "life" then
      current_resource_amount = game:get_life()
    else
      current_resource_amount = game:get_value(game:get_value(weapon .. "_cost_type")) or 0
      assert(type(current_resource_amount) == "number", "Weapon: " .. item.item_id .. " has a use cost savegame variable that is not a number")
    end

    if game:get_value(weapon .. "_cost_amount") > current_resource_amount then
      enough_resource = false
    else
      --Remove the appropriate resource
      if game:get_value(weapon .. "_cost_type") == "magic" then game:remove_magic(game:get_value(weapon .. "_cost_amount"))
      elseif game:get_value(weapon .. "_cost_type") == "life" then game:remove_life(game:get_value(weapon .. "_cost_amount"))
      else game:set_value(game:get_value(weapon .. "_cost_type"), (current_resource_amount - game:get_value(weapon .. "_cost_amount")))
      end
    end
    return enough_resource
  end


  --Custom attack state
  function item:get_attack_state()
    local state = sol.state.create()
    state:set_visible(true)
    state:set_can_control_direction(false)
    state:set_can_control_movement(false)
    state:set_gravity_enabled(true)
    state:set_can_come_from_bad_ground(true)
    state:set_can_be_hurt(true)
    state:set_can_use_sword(false)
    state:set_can_use_shield(false)
    state:set_can_use_item(false)
    state:set_can_interact(false)
    state:set_can_grab(false)
    state:set_can_push(false)
    state:set_can_pick_treasure(true)
    state:set_can_use_teletransporter(false)
    state:set_can_use_switch(true)
    state:set_can_use_stream(true)
    state:set_can_use_stairs(false)
    state:set_can_use_jumper(false)
    state:set_carried_object_action("throw")
    return state
  end

  return item

end


--Keep weapon anchored on hero, if they're moved by enemy attacks, streams, etc
local hero_meta = sol.main.get_metatable("hero")
hero_meta:register_event("on_position_changed", function(self, x, y, z)
  if self.weapon_entity and self.weapon_entity:exists() then
    self.weapon_entity:set_position(x, y, z)
  end
end)

--Remove the weapon if the player is hit, so it doesn't keep animating on its own
hero_meta:register_event("on_taking_damage", function(self)
  local hero = self
  if hero.weapon_entity and hero.weapon_entity:exists() then
    hero.weapon_entity:remove()
  end
end)


return weapon_factory

