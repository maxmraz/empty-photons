--Created by Max Mraz and J. Cournoyer, licensed with the MIT license
--This script manages what happens when a solforge weapon hits something.
--Alter this script in whatever way suits your game, default behavior provided will hurt enemies
--The solforge weapon is passed to the process_collision function as item, and the attack that collided as attack

local manager = {}

function manager:process_collision(props)
  local game = props.game
  local map = props.map
  local hero = props.hero
  local item = props.item --the item itself
  local attack = props.attack --the current attack
  local weapon_entity = props.weapon_entity --the custom entity of the weapon
  local other_entity = props.other_entity --the entity registering the sprite collision
  local weapon_sprite = props.weapon_sprite --the sprite of the weapon entity
  local other_sprite = props.other_sprite --the sprite of the other entity


  --Allow any entity to define its own behavior when getting hit:
  --For example, if you have a custom entity that can be destroyed, you can use this method
  if other_entity.react_to_solforge_weapon then other_entity:react_to_solforge_weapon(item) end


  --ENEMY
  if other_entity:get_type() == "enemy" then
    local attack_power = (game:get_value(item.item_id .. "_attack_power") or 1) + (attack.attack_power_bonus or 0)

    --Following function will take into account enemy's attack consequence settings,etc.
    manager:initiate_attack_consequence(other_entity, item.item_id, weapon_entity, attack_power)

   --Check if the enemy should push the hero back when struck; if so, move hero away from enemy slightly.
    if other_entity:get_push_hero_on_sword() then
      hero:stop_movement()
      local m = sol.movement.create"straight"
      m:set_speed(128)
      m:set_max_distance(64)
      m:set_angle(other_entity:get_angle(hero))
      m:start(hero)
    end

  end


  --DESTRUCTIBLE
  if other_entity:get_type() == "destructible" then
    local x,y,z = other_entity:get_position()

    --If it blows up, explode it
    if other_entity:get_can_explode() then
      map:create_explosion{x=x, y=y, layer=z}
      sol.audio.play_sound"explosion"
      if other_entity.on_exploded then other_entity:on_exploded() end
      other_entity:remove()

    --If it can be cut, cut it
    elseif other_entity:get_can_be_cut() then
      if other_entity:get_destruction_sound() then
        sol.audio.play_sound(other_entity:get_destruction_sound())
      end
      if other_entity:get_treasure() then
        local tname, tvar, tsave = other_entity:get_treasure()
        map:create_pickable{
          x=x, y=y, layer=z,
          treasure_name = tname, treasure_variant = tvar, treasure_savegame_variable = tsave,
        }
      end
      if other_sprite:has_animation("destroy") then
        other_sprite:set_animation("destroy", function()
          other_entity:remove()
        end)
      else
        other_entity:remove()
      end
      if other_entity.on_cut then other_entity:on_cut() end
    end
  end


  --SWITCH
  --Note: due to lack of accessor methods, there's no way to know if a switch is solid or arrow type
  --Therefore, this toggles ALL switches (other than walkable), even arrow type
  --Arrow type is a bit depricated anyway, as they only respond to built-in arrows. Custom arrows have no way to know either.
  if other_entity:get_type() == "switch"
    and not other_entity:is_walkable() then
    local switch = other_entity
    sol.audio.play_sound("switch")
    switch:set_activated(not switch:is_activated())
    if switch:is_activated() then
      other_sprite:set_animation("activated")
      if switch.on_activated then switch:on_activated() end
    else
      other_sprite:set_animation("inactivated")
      if switch.on_inactivated then switch:on_inactivated() end
    end
  end


  --Flip Crystal switches
  if other_entity:get_type() == "crystal" and not other_entity.solforge_activation_cooldown then
    sol.audio.play_sound("switch")
    map:change_crystal_state()
  end

end


function manager:initiate_attack_consequence(enemy, item_id, weapon_entity, attack_power)
  local game = enemy:get_game()
  local hero = game:get_hero()
  local attack_consequence = enemy:get_attack_consequence"sword"
  if (attack_consequence ~= "ignored") and (attack_consequence ~= "protected") then
    game:add_magic(game:get_value("sword_magic_regen_amount") or 15)
  end
  if enemy.process_hit then --allow enemies to define their own getting hurt methods
    enemy:process_hit(attack_power)
  elseif type(attack_consequence) == "number" then
    enemy:hurt(attack_power)
  elseif type(attack_consequence) == "function" then
    attack_consequence()
  else
    --If the attack_consequence is not a number or a function, then it is a string.
    if attack_consequence == "ignored" then
      --Do nothing. Enemy ignores the collision and damage.
    elseif attack_consequence == "protected" then
      if game:get_value(item_id .. "_attack_blocked_animation") and game:get_value(item_id .. "_attack_blocked_sound") then 
        weapon_entity:get_sprite():set_animation(game:get_value(item_id .. "_attack_blocked_animation"))
        sol.audio.play_sound(game:get_value(item_id .. "_attack_blocked_sound"))
      end
    elseif attack_consequence == "immobilized" then
      enemy:immobilize()
    elseif attack_consequence == "custom" then
      enemy:hurt(attack_power)
    end   --End the string if block.

  end   --End the type() if block.

end






return manager