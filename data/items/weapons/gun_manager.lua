local manager = {}

function manager:get_property_tables()
  local property_tables = {
    pistol = {
      ammo_cost = 15,
      fire_sound = "gunshot_pistol",
      damage = 3,
      speed = 450,
      bullet_sprite = "hero_projectiles/pistol",
      num_bullets = 1,
      spread = 0,
      range = 500,
      aim_animation = "aiming",
      aim_sound = "gun_aim_pistol",
    },
    shotgun = {
      ammo_cost = 35,
      fire_sound = "gunshot_shotgun",
      damage = 3,
      speed = 300,
      bullet_sprite = "hero_projectiles/shotgun",
      num_bullets = 5,
      spread = 75,
      range = 72,
      aim_animation = "aiming_shotgun",
      aim_sound = "gun_aim_shotgun",
    },
    sniper = {
      ammo_cost = 45,
      fire_sound = "gunshot_sniper",
      damage = 10,
      speed = 600,
      bullet_sprite = "hero_projectiles/sniper",
      num_bullets = 1,
      spread = 0,
      range = 600,
      aim_animation = "aiming_sniper",
      aim_sound = "gun_aim_shotgun",
    },
  }
  return property_tables
end


function manager:apply_properties_to_item(item, gun)
  local property_tables = manager:get_property_tables()
  for k, v in pairs(property_tables[gun]) do
    item[k] = v
  end
end


return manager