local item = ...
local game = item:get_game()

local gun_manager = require"items/weapons/gun_manager"

--local generation_offset_x = {[0]=0, [1]=0, [2]=0, [3]=0}
--local generation_offset_y = {[0]=-10, [1]=0, [2]=-10, [3]=-16}
local generation_offset_x = {[0]=0, [1]=0, [2]=0, [3]=0}
local generation_offset_y = {[0]=0, [1]=0, [2]=0, [3]=0}
local reticle
local reticle_sprite = "hud/reticle"

item.ammo_cost = 15
item.fire_sound = "door_closed"
item.damage = 2
item.speed = 450
item.bullet_sprite = "hero_projectiles/pistol"
item.num_bullets = 1
item.spread = 0
item.range = 700
item.aim_animation = "aiming"
item.aim_sound = "gun_aim_pistol"
item.no_ammo_sound = "gun_no_ammo"

function item:on_started()
  local savegame_variable = "possession_gun"
  item:set_savegame_variable(savegame_variable)
  item:set_assignable(true)
  item:set_ammo("_magic")
end

function item:on_using()
  local hero = game:get_hero()
  local map = game:get_map()
  local state = item:get_aim_state()
  local aim_key = "item_" .. (game:get_item_assigned(1) == item and 1 or 2)
  local equipped_gun = game:get_value("equipped_gun") or "pistol"
  gun_manager:apply_properties_to_item(item, equipped_gun)
  hero:start_state(state)
  hero:set_animation(item.aim_animation or "aiming")
  sol.audio.play_sound(item.aim_sound)
  reticle = hero:create_sprite(reticle_sprite)

  local enemies_in_range = {}
  local x,y,z = hero:get_position()
  for entity in map:get_entities_in_rectangle(x - item.range, y - item.range, item.range * 2, item.range * 2) do
    if entity:get_type() == "enemy" and entity:get_distance(hero) <= item.range then
      enemies_in_range[entity] = entity
    end
  end

  function state:on_command_released(command)
    if command == aim_key then
      hero:unfreeze()
      sol.audio.play_sound("gun_lower")
      sol.timer.start(map,100, function() item:set_finished() end)
    end
    state:update_reticle()
  end

  function state:on_command_pressed(command)
    if (command == "fire") or (command == "attack") then
      local ammo_amount_needed = item.ammo_cost * (game:get_value(equipped_gun .. "_ammo_cost_multiplier") or 100) / 100
      if ammo_amount_needed < 0 then ammo_amount_needed = 0 end
      if not item:try_spend_ammo(ammo_amount_needed) then
        sol.audio.play_sound(item.no_ammo_sound)
        item:set_finished()
        return
      end
      item:fire(item.num_bullets, item.spread)
      item:set_finished()
    elseif command == "right" then
      hero:set_direction(0)
    elseif command == "up" then
      hero:set_direction(1)
    elseif command == "left" then
      hero:set_direction(2)
    elseif command == "down" then
      hero:set_direction(3)
    end
    state:update_reticle()
  end

  function state:on_finished()
    local hero = game:get_hero()
    hero:remove_sprite(reticle)
  end

  function state:update_reticle()
    local hero = game:get_hero()
    local direction = item:get_direction_held()
    local angle = math.pi / 4 * direction
    local offset = 32
    local x, y = offset * math.cos(angle), offset * math.sin(angle) * -1
    y = y - 14
    reticle:set_xy(x, y)
  end

  state:update_reticle()
end


function item:stop_aiming()
  local hero = game:get_hero()
  hero:unfreeze()
  item:set_finished()
end


function item:fire(num_bullets, spread)
  num_bullets = num_bullets or 1
  spread = spread or 0
  spread = math.rad(spread)
  local hero = game:get_hero()
  local map = game:get_map()
  local direction = hero:get_direction()
  local x,y,z = hero:get_position()
  local step = 16
  local dx = {[0] = step, [1]=0, [2]= step * -1, [3]=0}
  local dy = {[0]=0, [1] = step * -1, [2]=0, [3]= step}
  for i = 0, num_bullets - 1 do
    local projectile = map:create_custom_entity{
      x = x + dx[direction] + generation_offset_x[direction],
      y = y + dy[direction] + generation_offset_y[direction],
      layer = z,
      direction = 0,
      width = 16, height = 8, 
      sprite = item.bullet_sprite,
      model = "hero_projectiles/bullet",
    }
    projectile.light_source_sprite = "hero_projectiles/pistol_glow"
    projectile.damage = item.damage
    projectile.speed = item.speed
    projectile.range = item.range
    projectile:set_can_traverse("hero", true)
    local aim_angle = item:get_direction_held() * math.pi / 4
    projectile:shoot(aim_angle - spread / 2 + spread / num_bullets * i)
  end

  sol.audio.play_sound(item.fire_sound)
end

function item:get_direction_held()
  local hero = game:get_hero()
  local up = game:is_command_pressed("up")
  local right = game:is_command_pressed("right")
  local left = game:is_command_pressed("left")
  local down = game:is_command_pressed("down")
  local direction = hero:get_direction() * 2
  if up and right then direction = 1
  elseif up and left then direction = 3
  elseif down and left then direction = 5
  elseif down and right then direction = 7
  elseif up then direction = 2
  elseif left then direction = 4
  elseif down then direction = 6
  elseif right then direction = 0
  end
  return direction
end


function item:get_aim_state()
  local hero = game:get_hero()
  local state = sol.state.create("aiming")
  state:set_can_control_direction(true)
  state:set_can_control_movement(false)
  state:set_gravity_enabled(true)
  state:set_can_come_from_bad_ground(true)
  state:set_can_be_hurt(true)
  state:set_can_use_sword(false)
  state:set_can_use_item(false)
  state:set_can_interact(false)
  state:set_can_grab(false)
  state:set_can_push(false)
  state:set_can_pick_treasure(true)
  state:set_can_use_teletransporter(true)
  state:set_can_use_switch(true)
  state:set_can_use_stream(true)
  state:set_can_use_stairs(true)
  state:set_can_use_jumper(true)
  state:set_carried_object_action("throw")

  return state
end
