local item = ...
local game = item:get_game()

function item:on_started()
  item:set_savegame_variable("possession_sniper_key")
  item:set_amount_savegame_variable("amount_sniper_key")
end

function item:on_obtained()
  item:add_amount(1)
end
