local map = ...
local game = map:get_game()

map:register_event("on_started", function()
  map:set_darkness_level({140,130,100})
  local world = map:get_world()
  game:set_world_rain_mode(world, "rain")

end)

map:register_event("on_opening_transition_finished", function()
  map:start_coroutine(function()
    hero:freeze()
    wait(1300)
    sol.audio.play_sound("hero_lands")
    hero:set_animation"jumping"
    local m = sol.movement.create"straight"
    m:set_angle(3 * math.pi / 2)
    m:set_max_distance(368)
    m:set_ignore_obstacles(true)
    m:set_speed(300)
    movement(m, hero)
    sol.audio.play_sound("hero_lands")
    hero:set_animation("landing", function()
      hero:set_animation("stopped")
    end)
    wait(2000)
    m = sol.movement.create"straight"
    m:set_max_distance(232)
    m:set_speed(90)
    m:set_angle(0)
    hero:set_direction(0)
    hero:set_animation"walking"
    m:start(hero)
    game:start_flash({0,0,0}, 40)
    wait(2000)
    hero:teleport("example/central", "start", "immediate")
    game:stop_flash(40)
  end)
end)
