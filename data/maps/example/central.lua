local map = ...
local game = map:get_game()

map:register_event("on_started", function()
  map:set_darkness_level({140,130,100})
  local world = map:get_world()
  game:set_world_rain_mode(world, "rain")
end)


function shopkeeper:on_interaction()
  local dialog = not game:get_value("example_shopkeeper_met") and "example.shopkeeper.1" or "example.shopkeeper." .. math.random(2, 4)
  game:start_dialog(dialog, function()
    game:set_value("example_shopkeeper_met", true)
    sol.menu.start(game, require"scripts/menus/cybershop")
  end)
end


--Central consoles
local function console_interaction(console_num)
  local key = game:get_item"collectibles/example_key"
  if game:get_value("example_central_console_" .. console_num .. "_activated") then
    game:start_dialog("example.central_console_already_unlocked")
    return
  end
  if not key:has_amount(1) then
    game:start_dialog("example.central_console")
  else
    game:start_dialog("example.central_console_unlocked", function(answer)
      if answer == 2 then
        game:set_value("example_central_console_" .. console_num .. "_activated", true)
        key:remove_amount(1)
        map:focus_on(final_door, function()
          map:open_doors("central_door_block_" .. console_num)
          sol.timer.start(map, 400, function()
            if game:get_value("example_central_console_1_activated") and game:get_value("example_central_console_2_activated") and game:get_value("example_central_console_3_activated") then
              map:open_doors"final_door"
            end
          end)
        end)
      end
    end)
  end
end

function central_console_1:on_interaction()
  console_interaction(1)
end

function central_console_2:on_interaction()
  console_interaction(2)
end

function central_console_3:on_interaction()
  console_interaction(3)
end



function checkpoint_sensor:on_activated()
  checkpoint_sensor:remove()
  game:save_checkpoint()
end
