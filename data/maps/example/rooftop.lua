local map = ...
local game = map:get_game()

map:register_event("on_started", function()
  map:set_darkness_level({140,130,100})
  local world = map:get_world()
  game:set_world_rain_mode(world, "rain")

  map:set_doors_open"boss_door"
end)

function boss_sensor:on_activated()
  boss_sensor:remove()
  if not uuv_boss then return end
  sol.audio.play_music"boss"
  map:close_doors"boss_door"
  for boss in map:get_entities"uuv_boss" do
    boss:set_enabled(true)
    boss:set_life(100)
    boss:start_aggro()
  end
end

for boss in map:get_entities("uuv_boss") do
function boss:on_dead()
  if not map:has_entities("uuv_boss") then
    map:open_doors("boss_door")
    sol.audio.play_music("rooftops")
    game:set_life(game:get_max_life())
    game:set_magic(game:get_max_magic())
  end
  map.postfight_scene_enabled = true
end
end

