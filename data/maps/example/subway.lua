local map = ...
local game = map:get_game()

map:register_event("on_started", function()
  map:set_darkness_level({100,90,70})

  map:set_doors_open"boss_door"
end)

function boss_sensor:on_activated()
  boss_sensor:remove()
  if not boss then return end
  sol.audio.play_music"boss"
  map:close_doors"boss_door"
  boss:set_enabled(true)
end

if boss then
  function boss:on_dead()
    map.postfight_scene_enabled = true
    map:open_doors"boss_door"
    sol.audio.play_music"rooftops"
    game:set_life(game:get_max_life())
    game:set_magic(game:get_max_magic())
  end
end

