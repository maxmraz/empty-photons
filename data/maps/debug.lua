local map = ...
local game = map:get_game()

map:register_event("on_started", function()
  map:set_darkness_level("seacity")
  map:set_darkness_level{250,240,210}
  local world = map:get_world()
  game:set_world_snow_mode(world, "snow")

  map:set_darkness_level("seacity_upper")
  map:set_fog("sunbeams")


  --game:set_value("ability_can_deflect_projectiles", true)

  --all powerful
  --[[
  game:set_max_life(30)
  game:set_max_magic(200)
  game:set_value("solforge_basic_sword_attack_power", 10)
  game:set_value("sniper_ammo_cost_multiplier", 10)
  game:set_value("pistol_ammo_cost_multiplier", 10)
  --]]


end)
