local map = ...
local game = map:get_game()

map:register_event("on_started", function()
  map:set_darkness_level("seacity")
  local world = map:get_world()
  game:set_world_snow_mode(world, "snow")
end)
