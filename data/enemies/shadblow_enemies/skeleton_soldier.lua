local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()

require("enemies/shadblow_enemies/lib/enemizer"):apply_behavior(enemy, {
  life = 20,
  max_poise = 12,
  detection_distance = 200,
  abandon_hero_distance = 400,
})
require("enemies/shadblow_enemies/attacks/melee"):apply_melee(enemy)
require("enemies/shadblow_enemies/attacks/ranged"):apply_ranged(enemy)

local thrust_attack = {
  windup_animation = "thrust_windup",
  attack_animation = "thrust_attack",
  weapon_sprite = "enemies/shadblow_enemies/weapons/skeleton_soldier_sword",
  weapon_windup_animation = "thrust_windup",
  weapon_attack_animation = "thrust_attack",
  damage = 4,
  type = "physical",
  recovery_delay = 1500,
}

local test_attack = {
  windup_animation = "thrust_windup",
  attack_animation = "thrust_attack",
  weapon_sprite = "enemies/trillium_enemies/weapons/hob_sword",
  weapon_windup_animation = "slash_windup",
  weapon_attack_animation = "slash_attack",
  damage = 2,
  type = "physical",
  recovery_delay = 800,
}

local combo_attack = { test_attack, thrust_attack }

local ranged_attack = {
  windup_animation = "crossbow_windup",
  windup_time = 800,
  projectile_model = "enemy_projectiles/generic_projectile",
  aim_type = "any",
  projectile_sprite = "entities/enemy_projectiles/generic_projectile",
  projectile_width = 8,
  projectile_height = 8,
  damage = 2,
  type = "physical",
  recovery_delay = 500,
}

local ranged_attack1 = {
  windup_animation = "crossbow_windup",
  windup_time = 800,
  projectile_model = "enemy_projectiles/generic_projectile",
  aim_type = "any",
  projectile_sprite = "entities/enemy_projectiles/generic_projectile",
  projectile_width = 8,
  projectile_height = 8,
  damage = 2,
  type = "physical",
  recovery_delay = 50,
}

local ranged_attack2 = {
  windup_animation = "crossbow_windup",
  windup_time = 100,
  projectile_model = "enemy_projectiles/generic_projectile",
  aim_type = "any",
  projectile_sprite = "entities/enemy_projectiles/generic_projectile",
  projectile_width = 8,
  projectile_height = 8,
  damage = 2,
  type = "physical",
  recovery_delay = 50,
}

local ranged_attack3 = {
  windup_animation = "crossbow_windup",
  windup_time = 100,
  projectile_model = "enemy_projectiles/generic_projectile",
  aim_type = "any",
  projectile_sprite = "entities/enemy_projectiles/generic_projectile",
  projectile_width = 8,
  projectile_height = 8,
  damage = 2,
  type = "physical",
  recovery_delay = 900,
}


local ranged_combo = {ranged_attack1, ranged_attack2, ranged_attack3}


function enemy:decide_action()
  local distance = enemy:get_distance(hero)
  if distance >= enemy.abandon_hero_distance then
    enemy:return_to_idle()
  elseif distance >= 80 and enemy:has_los(hero) then
    if math.random(1,3) < 2 then enemy:ranged_combo(ranged_combo)
    else enemy:ranged_attack(ranged_attack) end
  else
    enemy:approach_then_attack({
      approach_duration = 1500,
      attack_function = function()
        if math.random(1,4) < 2 then
          enemy:melee_attack(thrust_attack)
        else
          enemy:melee_attack(test_attack)
        end
      end
    })
  end
end
