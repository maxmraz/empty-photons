local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()
local sprite
local movement

enemy.exp = 0

function enemy:on_created()
  sprite = enemy:create_sprite("enemies/" .. enemy:get_breed())
  enemy:set_life(1)
  enemy:set_damage(1)
  enemy:set_obstacle_behavior("flying")
  --local bubbles = enemy:create_sprite("enemies/cybersea_enemies/uuv_bubble_jet")
  --bubbles:set_xy(0, -16)
end

function enemy:on_restarted()
  movement = sol.movement.create("target")
  movement:set_target(hero)
  movement:set_speed(60)
  movement:start(enemy)
end
