local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()

enemy.exp = 100

require("enemies/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = 120,
  detection_distance = 400,
  abandon_hero_distance = 800,
  stunlock_limit = 5,
  stunlock_reset_rate = 500,
})
require("enemies/trillium_enemies/attacks/ranged"):apply_ranged(enemy)
require("enemies/trillium_enemies/attacks/spoke_bullet"):apply_behavior(enemy)


local ranged_attack = {
  windup_animation = "aiming",
  windup_time = 700,
  attack_animation = "shooting",
  attack_sound = "bot_gun",
  projectile_model = "enemy_projectiles/generic_projectile",
  generic_projectile_type = "energy",
  aim_type = "any",
  projectile_sprite = "entities/enemy_projectiles/energy_bullet",
  projectile_width = 16,
  projectile_height = 16,
  damage = 2,
  type = "physical",
  recovery_delay = 500,
  recovery_animation = "aiming",
}

local ranged_1 = {
  windup_animation = "aiming",
  windup_sound = "bot_charge",
  windup_time = 1000,
  attack_animation = "shooting",
  attack_sound = "bot_gun",
  projectile_model = "enemy_projectiles/generic_projectile",
  generic_projectile_type = "energy",
  aim_type = "any",
  projectile_sprite = "entities/enemy_projectiles/energy_bullet",
  projectile_width = 16,
  projectile_height = 16,
  damage = 2,
  type = "physical",
  recovery_delay = 100,
  recovery_animation = "aiming",
}

local ranged_2 = {
  windup_animation = "aiming",
  windup_time = 50,
  attack_animation = "shooting",
  attack_sound = "bot_gun",
  projectile_model = "enemy_projectiles/generic_projectile",
  generic_projectile_type = "energy",
  aim_type = "any",
  projectile_sprite = "entities/enemy_projectiles/energy_bullet",
  projectile_width = 16,
  projectile_height = 16,
  damage = 2,
  type = "physical",
  recovery_delay = 100,
  recovery_animation = "aiming",
}

local ranged_3 = {
  windup_animation = "aiming",
  windup_time = 50,
  attack_animation = "shooting",
  attack_sound = "bot_gun",
  projectile_model = "enemy_projectiles/generic_projectile",
  generic_projectile_type = "energy",
  aim_type = "any",
  projectile_sprite = "entities/enemy_projectiles/energy_bullet",
  projectile_width = 16,
  projectile_height = 16,
  damage = 2,
  type = "physical",
  recovery_delay = 1200,
  recovery_animation = "aiming",
}

local ranged_combo = {
  ranged_1, ranged_2, ranged_3
}

local spoke_attack = {
  num_projectiles = 14,
  windup_animation = "dashing",
  windup_time = 500,
  attack_animation = "crouching",
  attack_sound = "running_obstacle",
  projectile_model = "enemy_projectiles/generic_projectile",
  projectile_sprite = "entities/enemy_projectiles/generic_projectile",
  generic_projectile_type = "energy",
  damage = 2,
  offset_radius = 8,
  recovery_animation = "crouching",
  recovery_time = 500,
}


function enemy:decide_action()
  local sprite = enemy:get_sprite()
  sprite:set_animation("aiming")
  sprite:set_direction(enemy:get_direction4_to(hero))
  sol.timer.start(enemy, 200, function()
    local rand = math.random(1, 16)
      if rand == 1 then
        enemy:pattern_snipe()
      elseif rand <= 5 then
        enemy:shockwave()
      else
        enemy:teleport_then_attack()
      end
  end)
end



function enemy:teleport_then_attack()
  local num_teleport_spots = 6
  local rand = math.random(1, num_teleport_spots)
  if rand == enemy.last_teleport_position then rand = (rand + 1) % num_teleport_spots + 1 end
  enemy.last_teleport_position = rand
  local target_entity = map:get_entity("boss_ground_position_" .. rand)
  local sprite = enemy:get_sprite()
  sprite:set_animation("teleporting")
  sol.timer.start(enemy, 100, function()
    enemy:set_position(target_entity:get_position())
    sol.timer.start(enemy, 100, function()
      enemy:ranged_combo(ranged_combo)
    end)
  end)
end


function enemy:pattern_snipe()
  local rand = math.random(1, 2)
  local target_entity = map:get_entity("boss_platform_position_" .. rand)
  local sprite = enemy:get_sprite()
  sprite:set_animation("teleporting")
  sol.timer.start(enemy, 80, function()
    enemy:set_position(target_entity:get_position())
    sol.timer.start(enemy, 100, function()
      sprite:set_direction(3)
      sprite:set_animation"aiming"
      sol.audio.play_sound("bot_charge")
      sol.timer.start(enemy, 400, function()
        local default_spin_direction = rand == 1 and 1 or -1
        enemy.bullet_spray_angle = math.pi / 2 * 3
        for i = 1, 3 do
          local spin_mod = (i % 2 == 0) and -1 or 1
          sol.timer.start(enemy, 1000 * (i - 1), function()
            enemy:spray_bullets(default_spin_direction * spin_mod)
          end)
        end

        sol.timer.start(enemy, 4000, function()
          enemy:decide_action()
        end)
      end)
    end)
  end)
end


function enemy:spray_bullets(spin_direction) --direction is 1 or -1, counter clockwise or clockwise
  spin_direction = spin_direction or 1
  local num_bullets = 6
  local spread_angle = 90
  local sprite = enemy:get_sprite()
  local angle_step = math.rad(spread_angle) / (num_bullets - 1)
  local x, y, z = enemy:get_position()
  local direction = sprite:get_direction()
  local step = 16
  local projectile_offset_x = 0
  local projectile_offset_y = 0
  local dx = {[0] = step + projectile_offset_x, [1]=0, [2]= step * -1 - projectile_offset_x, [3]=0}
  local dy = {[0]=0, [1] = step * -1 - projectile_offset_y, [2]=0, [3]= step + projectile_offset_y}

  local bullet_delay = 100

  for i = 1, num_bullets do
    local current_angle = enemy.bullet_spray_angle
    sol.timer.start(enemy, bullet_delay * (i - 1), function()
      sprite:set_animation("shooting", "aiming")
      local projectile = map:create_custom_entity{
        x = x + dx[direction],
        y = y + dy[direction],
        layer = z,
        width = 16, height = 16, direction = 0, 
        model = "enemy_projectiles/generic_projectile", sprite = "entities/enemy_projectiles/energy_bullet",
      }
      projectile.ignore_obstacles = true
      projectile:set_projectile_type("energy")
      projectile.damage = 2
      projectile.damage_type = "physical"
      projectile.firing_entity = enemy
      projectile:shoot(current_angle)
      sol.audio.play_sound("bot_gun")
    end)
    enemy.bullet_spray_angle = enemy.bullet_spray_angle + (angle_step * spin_direction)
  end

end


function enemy:shockwave()
  local target_entity = map:get_entity("boss_ground_position_center")
  local sprite = enemy:get_sprite()
  sprite:set_animation("teleporting")
  sol.timer.start(enemy, 100, function()
    enemy:set_position(target_entity:get_position())
    sprite:set_direction(3)
    sol.audio.play_sound"bot_charging"
    enemy:spoke_bullet_attack(spoke_attack)
  end)
end



function enemy:stunlock_break()
  enemy:pattern_snipe()
end


enemy:register_event("on_created", function()
  enemy:set_hurt_style"boss"
end)
