local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()
local sprite
local movement

enemy.exp = 0

function enemy:on_created()
  sprite = enemy:create_sprite("enemies/" .. enemy:get_breed())
  enemy:set_life(1)
  enemy:set_damage(1)
  enemy:set_pushed_back_when_hurt(false)
  enemy:set_obstacle_behavior"flying"
  enemy:set_attacking_collision_mode("overlapping")
end


function enemy:on_restarted()
  enemy:set_invincible()
  enemy:set_can_attack(false)
  sprite:set_animation("descending", function()
    sprite:set_animation"walking"
    enemy:set_can_attack(true)
  end)
end

function enemy:go(direction4, duration)
  local m = sol.movement.create"straight"
  m:set_angle(direction4 * math.pi / 2)
  m:set_speed(120)
  m:set_max_distance(duration or 100)
  m:set_ignore_obstacles(true)
  m:start(enemy, function()
    sprite:set_animation("ascending", function() enemy:remove() end)
  end)
end
