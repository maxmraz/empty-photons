local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()
local sprite
local movement

local ARM_LENGTH = 48
local SEGMENT_SPACING = 4


function enemy:on_created()
  sprite = enemy:create_sprite("enemies/" .. enemy:get_breed())
  enemy:create_arms(1)
end



function enemy:on_restarted()
  enemy:set_can_attack(false)

--Set a simple movement on the claw so we can test how it works
  for i, claw in pairs(enemy.claws) do

    local angle = math.rad(math.random(230, 310))
    local perp_angle = angle + math.pi / 2

    local mov = sol.movement.create"straight"
    mov:set_angle(angle)
    mov:set_max_distance(ARM_LENGTH - 8)
    enemy:move_claw(claw, mov)

    local counter = 1
    sol.timer.start(enemy, 1500, function()
      local m = sol.movement.create"straight"
      m:set_angle(math.pi * counter + perp_angle)
      m:set_speed(25)
      enemy:move_claw(claw, m)
      counter = counter + 1 % 2
      return true
    end)
  end
--]]
end



function enemy:move_claw(claw, movement)
  movement:start(claw)

  claw.movement = movement

  function claw.movement:on_changed()
    
  end

  function claw.movement:on_position_changed()
    claw:set_direction(claw.movement:get_direction4())
    enemy:update_arms()
  end

end


function enemy:update_arms()
  for _, claw in ipairs(enemy.claws) do
    local cx, cy = claw:get_xy()
    local sx, sy = sprite:get_xy() --anchor point for this arm
    --Update arm segment positions
    for i, node in ipairs(claw.nodes) do
      local previous_node = claw.nodes[i + 1]
      if not previous_node then --this is the last node, the next would be the claw, so we'll create a previous node with claw data
        previous_node = {last_x = claw.last_x, last_y = claw.last_y}
      end
      local angle = sol.main.get_angle(sx, sy, previous_node.last_x, previous_node.last_y) --angle from anchor point to previous node's previous position
      local distance = sol.main.get_distance(cx, cy, sx, sy) / #claw.nodes * i
      local x = math.floor(distance * math.cos(angle) + sx)
      local y = math.floor(distance * math.sin(angle) + sy)
      node:set_xy(x, y)
      node.last_x, node.last_y = node.current_x, node.current_y
      node.current_x, node.current_y = x, y
    end
    --Update last position
    claw.last_x, claw.last_y = claw.current_x, claw.current_y
    claw.current_x, claw.current_y = cx, cy
  end
end



function enemy:create_arms(num_arms)
  local num_nodes = ARM_LENGTH / SEGMENT_SPACING
print("NUM NODES:", num_nodes)
  num_arms = num_arms or 6
  enemy.claws = {}
  for i = 1, num_arms do
    local claw = enemy:create_sprite("enemies/cybersea_enemies/mecha_squid_claw")
    claw.last_x, claw.last_y = 0, 0
    claw.current_x, claw.current_y = 0, 0
    claw.nodes = {}
    for i = 1, num_nodes do
      local node = enemy:create_sprite("enemies/cybersea_enemies/mecha_squid_arm_segment")
      node.last_x, node.last_y = 0, 0
      node.current_x, node.current_y = 0, 0
      claw.nodes[i] = node
    end

    enemy.claws[i] = claw
    enemy:bring_sprite_to_front(claw)
  end
  enemy:bring_sprite_to_front(sprite)
end
