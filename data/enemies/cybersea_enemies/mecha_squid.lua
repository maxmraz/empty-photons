local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()

enemy.exp = 20

require("enemies/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = 200,
  detection_distance = 900,
  abandon_hero_distance = 1500,
  stunlock_limit = 6,
  stunlock_reset_rate = 700,
})
require("enemies/trillium_enemies/attacks/ranged"):apply_ranged(enemy)
require("enemies/trillium_enemies/attacks/spoke_bullet"):apply_behavior(enemy)

  
enemy:register_event("on_created", function()
  enemy:set_size(64, 48)
  enemy:set_origin(32, 45)
  enemy:set_hurt_style"boss"
  enemy:set_obstacle_behavior"flying"
end)


local radial_shot = {
  num_projectiles = 6,
  damage = 2,
  windup_animation = "radial_aiming",
  attack_animation = "radial_firing",
  attack_sound = "bot_laser",
  projectile_model = "enemy_projectiles/generic_projectile",
  generic_projectile_type = "energy",
  projectile_sprite = "hero_projectiles/sniper",
  projectile_speed = 180,
  rotational_sprite = true,  
  offset_radius = 32,
  origin_offset = {x = 0, y = -32},
}


local function get_missile_attack()
  local attack = {
    windup_time = 100,
    windup_animation = "missile_firing",
    attack_animation = "missile_firing",
    attack_sound = "bot_gun",
    projectile_model = "enemy_projectiles/generic_projectile",
    generic_projectile_type = "missile",
    aim_type = "any",
    projectile_sprite = "entities/enemy_projectiles/missile",
    projectile_width = 16,
    projectile_height = 16,
    damage = 3,
    type = "physical",
    recovery_delay = 100,
  }
  return attack
end

local missile_1 = get_missile_attack()
missile_1.windup_time = 1000
local missile_2 = get_missile_attack()
local missile_3 = get_missile_attack()
local missile_4 = get_missile_attack()
local missile_5 = get_missile_attack()
local missile_6 = get_missile_attack()
local missile_7 = get_missile_attack()
missile_7.recovery_delay = 1500

local missile_combo_1 = {missile_1, missile_5, missile_6, missile_7}
local missile_combo_2 = {missile_1, missile_2, missile_5, missile_6, missile_7}
local missile_combo_3 = {missile_1, missile_2, missile_3, missile_5, missile_6, missile_7}
local missile_combo_4 = {missile_1, missile_2, missile_3, missile_4, missile_5, missile_6, missile_7}
local missile_combos = {missile_combo_1, missile_combo_2, missile_combo_3, missile_combo_4}


local function get_blaster_attack()
  local attack = {
    windup_animation = "blaster_aiming",
    windup_sound = "bot_piston",
    windup_time = 150,
    attack_animation = "blaster_firing",
    attack_sound = "bot_laser",
    projectile_model = "enemy_projectiles/generic_projectile",
    generic_projectile_type = "energy",
    aim_type = "any",
    projectile_sprite = "hero_projectiles/sniper",
    projectile_width = 16,
    projectile_height = 16,
    damage = 2,
    type = "physical",
    recovery_delay = 50,
    recovery_animation = "blaster_aiming",
  }
  return attack
end

local blaster_attack_1 = get_blaster_attack()
blaster_attack_1.windup_time = 600
local blaster_attack_2 = get_blaster_attack()
local blaster_attack_3 = get_blaster_attack()
blaster_attack_3.recovery_delay = 700

local blaster_combo = { blaster_attack_1, blaster_attack_2, blaster_attack_3}



function enemy:decide_action()
  local sprite = enemy:get_sprite()
  local distance = enemy:get_distance(hero)
  local has_los = enemy:has_los(hero)
  sprite:set_animation"walking"
  if distance > 200 then
    enemy.just_attacked = true
    rand = math.random(1, 5)
    if rand <= 4 then
      enemy:ranged_combo(blaster_combo)
    else
      enemy:ranged_combo(missile_combos[math.random(1,4)])
    end
  elseif enemy.just_attacked then
    enemy.just_attacked = false
    enemy:drift()
  else
    enemy.just_attacked = true
    local rand = math.random(1, 12)
    local x, y, z = enemy:get_position()
    local hx, hy, hz = hero:get_position()

    if rand > 4 and y <= (hy + 48) and distance < 180 then
      enemy:beam_sweep()
    elseif rand <= 3 then
      enemy:spoke_bullet_attack(radial_shot)
    elseif rand <= 6 then
      enemy:ranged_combo(missile_combos[math.random(1,4)])
    elseif rand <= 7 then
      enemy:ranged_combo(blaster_combo)
    elseif rand <= 12 then
      enemy:mine_attack()
    else
      enemy:decide_action()
    end
  end
end


function enemy:stunlock_break()
  local m = sol.movement.create"straight"
  m:set_angle(hero:get_angle(enemy))
  m:set_speed(200)
  m:set_max_distance(256)
  m:start(enemy, function()
    enemy:ranged_combo(blaster_combo)
  end)
  function m:on_obstacle_reached()
    enemy:ranged_combo(blaster_combo)
  end
end



function enemy:drift()
  local m = sol.movement.create"straight"
  m:set_angle(math.rad(math.random(0,360)))
  m:set_max_distance(math.random(40,128))
  m:set_speed(110)
  m:start(enemy, function() enemy:decide_action() end)
  function enemy:on_obstacle_reached() enemy:decide_action() end
end


function enemy:beam_sweep()
  local sprite = enemy:get_sprite()
  local m = sol.movement.create"straight"
  m:set_angle(math.pi / 2)
  m:set_speed(120)
  m:set_max_distance(48)
  local function beam_attack_function()
    enemy:stop_movement()
    sprite:set_animation"beam_sweeping"
    sol.audio.play_sound("bot_charge")
    sol.timer.start(enemy, 800, function()
      sol.audio.play_sound("bot_beam")
      enemy:make_beam(-32, -60)
      enemy:make_beam(32, -60)
      sol.timer.start(enemy, 1600, function()
        enemy:decide_action()
      end)
    end)
  end

  m:start(enemy, function()
    beam_attack_function()
  end)
  m.on_obstacle_reached = beam_attack_function

end


function enemy:make_beam(orig_x, orig_y)
  local attack_radius = 196
  local x, y, z = enemy:get_position()

  local beam = map:create_custom_entity{
    x = x + orig_x, y = y + orig_y, layer=z,
    direction=0, width=16, height=16,
    model = "enemy_projectiles/beam",
    sprite = "enemies/cybersea_enemies/mech_beam",
  }
  enemy.attack_entities[beam] = beam
  beam.damage = 5
  local beam_sprite = beam:get_sprite()

  local target_entity = map:create_custom_entity{ x=x, y=y, layer=z, direction=0, width=16, height=16, }
  local m = sol.movement.create"circle"
  m:set_center(x, y)
  m:set_radius(attack_radius)
  m:set_angular_speed(1.4)
  m:set_clockwise(beam:get_position() < x)
  m:set_angle_from_center(beam:get_angle(enemy))
  m:set_duration(1500)
  m:set_ignore_obstacles()
  m:start(target_entity, function()
    target_entity:remove()
    beam:remove()
  end)
  function m:on_position_changed()
    beam_sprite:set_rotation(beam:get_angle(target_entity))
    local scale = beam:get_distance(target_entity) / 160
    beam_sprite:set_scale(scale, 1)
  end
end


function enemy:mine_attack()
  local sprite = enemy:get_sprite()
  enemy:stop_movement()
  sprite:set_animation"slam_windup"
  sol.timer.start(enemy, 800, function()
    sprite:set_animation("slam_attack")
    sol.audio.play_sound"running_obstacle"
    enemy:lay_mines()
    sol.timer.start(enemy, 800, function()
      sprite:set_animation"walking"
      enemy:decide_action()
    end)
  end)
end

function enemy:lay_mines()
  local deviation = 32
  local num_mines = 8
  local frequency = 100
  local targeting_speed = 240
  local x, y, z = enemy:get_position()
  local target_entity = map:create_custom_entity{ x=x, y=y, layer=z, direction=0, width=16, height=16, }
  local m = sol.movement.create"straight"
  m:set_angle(enemy:get_angle(hero))
  m:set_speed(targeting_speed)
  m:set_smooth(false)
  m:set_ignore_obstacles()
  m:start(target_entity, function() target_entity:remove() end)
  function m:on_obstacle_reached() target_entity:remove() end
  local laid_mines = 0
  sol.timer.start(map, frequency, function()
    local x, y, z = target_entity:get_position()
    local mine = map:create_custom_entity{
      x = x + math.random(deviation * -1, deviation), y = y + math.random(deviation * -1, deviation), layer = z,
      width = 16, height = 16, direction = 0,
      model = "enemy_projectiles/mine",
      sprite = "entities/enemy_projectiles/diamond_blast",
    }
    mine.damage = 4
    mine:set_fuse(500)
    laid_mines = laid_mines + 1
    if laid_mines < num_mines then
      return true
    end
  end)
end



function enemy:process_hit(attack_power)
  local HURT_TIME = 150
  if enemy:get_life() <= 0 then return end
  if enemy:get_attack_consequence("sword") == "ignored" then
    return
  end
  local sprite = enemy:get_sprite()
  sol.audio.play_sound"enemy_hurt"
--[[
  if sprite:has_animation("hurt") then
    sprite:set_animation"hurt"
    sol.timer.start(enemy:get_map(), HURT_TIME, function()
      sprite:set_animation"walking"
    end)
  else
    sprite:set_blend_mode"add"
    sol.timer.start(enemy:get_map(), HURT_TIME, function()
        sprite:set_blend_mode"blend"
    end)
  end --]]
  sprite:set_blend_mode"add"
  sol.timer.start(enemy:get_map(), HURT_TIME, function()
      sprite:set_blend_mode"blend"
  end)
  enemy:remove_life(attack_power)
  enemy:on_hurt()
end


