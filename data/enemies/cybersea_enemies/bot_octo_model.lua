local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()

enemy.exp = 5

require("enemies/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = 8,
  detection_distance = 200,
  abandon_hero_distance = 400,
})
require("enemies/trillium_enemies/attacks/melee"):apply_melee(enemy)
require("enemies/trillium_enemies/attacks/ranged"):apply_ranged(enemy)


local ranged_attack = {
  damage = 4,
  windup_animation = "aiming",
  windup_sound = "bot_charging",
  windup_time = 800,
  attack_animation = "shooting",
  attack_sound = "bot_laser",
  projectile_model = "enemy_projectiles/generic_projectile",
  generic_projectile_type = "energy",
  aim_type = "any",
  projectile_sprite = "entities/enemy_projectiles/energy_bullet",
  projectile_width = 16,
  projectile_height = 16,
  damage = 2,
  type = "physical",
  recovery_delay = 1000,
}



local ranged_combo = {ranged_attack1, ranged_attack2, ranged_attack3}

function enemy:decide_action()
  if not enemy.random_movement_count then enemy.random_movement_count = 0 end
  local distance = enemy:get_distance(hero)
  if distance >= enemy.abandon_hero_distance then
    enemy:return_to_idle()
  elseif distance >= 24 and enemy:has_los(hero) then
    enemy.random_movement_count = 0
    sol.timer.start(enemy, math.random(10,150), function()
      enemy:ranged_attack(ranged_attack)
    end)
  elseif enemy.random_movement_count < 6 then
    enemy:move_randomly()
  else
    enemy:be_watchful()
  end
end

function enemy:move_randomly()
  local sprite = enemy:get_sprite()
  sprite:set_animation"walking"
  enemy.random_movement_count = enemy.random_movement_count + 1
  local m = sol.movement.create"straight"
  m:set_max_distance(math.random(16, 40))
  m:set_speed(80)
  m:set_angle(math.rad(math.random(0, 360)))
  m:start(enemy, function()
    sprite:set_animation"stopped"
    enemy:decide_action()
  end)
  function m:on_obstacle_reached()
    sprite:set_animation"stopped"
    enemy:decide_action()
  end
end

function enemy:be_watchful()
  enemy:get_sprite():set_direction(enemy:get_direction4_to(hero))
  sol.timer.start(enemy, 100, function()
    enemy:decide_action()
  end)
end
