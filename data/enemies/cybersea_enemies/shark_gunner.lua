local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()

enemy.exp = 30

enemy.diagonal_facing = true

require("enemies/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = 15,
  detection_distance = 200,
  abandon_hero_distance = 400,
  stunlock_limit = 5,
  stunlock_reset_rate = 800,
})
require("enemies/trillium_enemies/attacks/ranged"):apply_ranged(enemy)



local ranged_attack = {
  windup_animation = "gun_windup",
  windup_time = 700,
  attack_animation = "gun_fire",
  attack_sound = "bot_gun",
  projectile_model = "enemy_projectiles/generic_projectile",
  generic_projectile_type = "energy",
  aim_type = "any",
  projectile_sprite = "entities/enemy_projectiles/energy_bullet",
  projectile_width = 16,
  projectile_height = 16,
  damage = 2,
  type = "physical",
  recovery_delay = 500,
}

local ranged_1 = {
  windup_animation = "gun_windup",
  --windup_sound = "bot_charge",
  windup_time = 1000,
  attack_animation = "gun_fire",
  attack_sound = "bot_gun",
  projectile_model = "enemy_projectiles/generic_projectile",
  generic_projectile_type = "energy",
  aim_type = "any",
  projectile_sprite = "entities/enemy_projectiles/energy_bullet",
  projectile_width = 16,
  projectile_height = 16,
  damage = 2,
  type = "physical",
  recovery_delay = 100,
}

local ranged_2 = {
  windup_animation = "gun_windup",
  windup_time = 50,
  attack_animation = "gun_fire",
  attack_sound = "bot_gun",
  projectile_model = "enemy_projectiles/generic_projectile",
  generic_projectile_type = "energy",
  aim_type = "any",
  projectile_sprite = "entities/enemy_projectiles/energy_bullet",
  projectile_width = 16,
  projectile_height = 16,
  damage = 2,
  type = "physical",
  recovery_delay = 100,
}

local ranged_3 = {
  windup_animation = "gun_windup",
  windup_time = 50,
  attack_animation = "gun_fire",
  attack_sound = "bot_gun",
  projectile_model = "enemy_projectiles/generic_projectile",
  generic_projectile_type = "energy",
  aim_type = "any",
  projectile_sprite = "entities/enemy_projectiles/energy_bullet",
  projectile_width = 16,
  projectile_height = 16,
  damage = 2,
  type = "physical",
  recovery_delay = 1500,
}

local ranged_combo = {
  ranged_1, ranged_2, ranged_3
}


function enemy:decide_action()
  local distance = enemy:get_distance(hero)
  local rand = math.random(1, 4)
  local has_los = enemy:has_los(hero)
  if distance >= enemy.abandon_hero_distance then
    enemy:return_to_idle()
  elseif distance >= 100 and not has_los then
    enemy:approach_then_attack({
      dist_threshold = 90,
      attack_duration = 1000,
      attack_function = function()
        enemy:ranged_combo(ranged_combo)
      end
    })
  elseif distance < 100 and not has_los then
    local m = sol.movement.create"straight"
    m:set_max_distance(64)
    m:set_speed(75)
    m:set_angle(enemy:get_angle(hero) + math.pi / 2 * (math.random(1,2) == 1 and 1 or -1))
    m:start(enemy, function() enemy:decide_action() end)
    function m:on_obstacle_reached() enemy:decide_action() end
  elseif distance >= 64 and has_los then
    enemy:ranged_combo(ranged_combo)
--[[
  elseif distance < 64 then
    local angle = hero:get_angle(enemy)
    local offset = 16
    local obstacle = enemy:test_obstacles(offset * math.cos(angle), offset * math.sin(angle))
    if obstacle then angle = angle + math.pi / 2 * (math.random(1,2) == 1 and 1 or -1) end
    local m = sol.movement.create("straight")
    m:set_max_distance(64)
    m:set_speed(100)
    m:start(enemy, function() enemy:decide_action() end)
    function m:on_obstacle_reached() enemy:decide_action() end
--]]
  else
    sol.timer.start(enemy, 500, function()
      enemy:ranged_combo(ranged_combo)
    end)
  end
end




function enemy:stunlock_break()
  sol.timer.stop_all(enemy)
  enemy:set_invincible()
  local m = sol.movement.create("straight")
  enemy.lock_facing = true
  local react = function()
    enemy.lock_facing = false
    enemy:ranged_attack(ranged_2)
    enemy:set_default_attack_consequences()
  end
  m:set_angle(hero:get_angle(enemy))
  m:set_speed(140)
  m:set_max_distance(48)
  m:start(enemy, react)
  m.on_obstacle_reached = react
end
