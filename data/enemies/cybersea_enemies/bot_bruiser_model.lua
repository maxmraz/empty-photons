local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()

enemy.exp = 20

require("enemies/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = 30,
  detection_distance = 200,
  abandon_hero_distance = 400,
  stunlock_limit = 5,
  stunlock_reset_rate = 800,
})
require("enemies/trillium_enemies/attacks/melee"):apply_melee(enemy)
require("enemies/trillium_enemies/attacks/ranged"):apply_ranged(enemy)


local backslash_attack = {
  windup_delay = 300,
  windup_animation = "backslash_windup",
  attack_animation = "backslash_attack",
  attack_movement_distance = 48,
  weapon_sprite = "enemies/trillium_enemies/weapons/hob_sword",
  weapon_windup_animation = "hidden",
  weapon_attack_animation = "backslash_attack",
  damage = 1,
  type = "physical",
  recovery_delay = 200,
}


local combo_1 = {
  windup_delay = 700,
  windup_animation = "slash_windup",
  attack_animation = "slash_attack",
  attack_movement_distance = 16,
  weapon_sprite = "enemies/trillium_enemies/weapons/hob_sword",
  weapon_windup_animation = "hidden",
  weapon_attack_animation = "slash_attack",
  damage = 2,
  type = "physical",
  recovery_delay = 20,
}

local combo_2 = {
  windup_delay = 50,
  windup_animation = "backslash_windup",
  attack_animation = "backslash_attack",
  attack_movement_distance = 16,
  weapon_sprite = "enemies/trillium_enemies/weapons/hob_sword",
  weapon_windup_animation = "hidden",
  weapon_attack_animation = "backslash_attack",
  damage = 2,
  type = "physical",
  recovery_delay = 50,
}

local combo_2b = {
  windup_delay = 50,
  windup_animation = "backslash_windup",
  attack_animation = "backslash_attack",
  attack_movement_distance = 16,
  weapon_sprite = "enemies/trillium_enemies/weapons/hob_sword",
  weapon_windup_animation = "hidden",
  weapon_attack_animation = "backslash_attack",
  damage = 2,
  type = "physical",
  recovery_delay = 600,
}


local combo_3 = {
  windup_delay = 200,
  windup_animation = "slash_windup",
  attack_animation = "slash_attack",
  attack_movement_distance = 16,
  weapon_sprite = "enemies/trillium_enemies/weapons/hob_sword",
  weapon_windup_animation = "hidden",
  weapon_attack_animation = "slash_attack",
  damage = 3,
  type = "physical",
  recovery_delay = 800,
}


local slash_combo = {combo_1, combo_2, combo_3}
local short_combo = {combo_1, combo_2b}


local ranged_attack = {
  windup_animation = "gun_windup",
  windup_time = 700,
  attack_animation = "gun_fire",
  attack_sound = "bot_gun",
  projectile_model = "enemy_projectiles/generic_projectile",
  generic_projectile_type = "energy",
  aim_type = "any",
  projectile_sprite = "entities/enemy_projectiles/energy_bullet",
  projectile_width = 16,
  projectile_height = 16,
  damage = 2,
  type = "physical",
  recovery_delay = 500,
}

local ranged_1 = {
  windup_animation = "gun_windup",
  windup_sound = "bot_charge",
  windup_time = 1000,
  attack_animation = "gun_fire",
  attack_sound = "bot_gun",
  projectile_model = "enemy_projectiles/generic_projectile",
  generic_projectile_type = "energy",
  aim_type = "any",
  projectile_sprite = "entities/enemy_projectiles/energy_bullet",
  projectile_width = 16,
  projectile_height = 16,
  damage = 2,
  type = "physical",
  recovery_delay = 100,
}

local ranged_2 = {
  windup_animation = "gun_windup",
  windup_time = 50,
  attack_animation = "gun_fire",
  attack_sound = "bot_gun",
  projectile_model = "enemy_projectiles/generic_projectile",
  generic_projectile_type = "energy",
  aim_type = "any",
  projectile_sprite = "entities/enemy_projectiles/energy_bullet",
  projectile_width = 16,
  projectile_height = 16,
  damage = 2,
  type = "physical",
  recovery_delay = 100,
}

local ranged_3 = {
  windup_animation = "gun_windup",
  windup_time = 50,
  attack_animation = "gun_fire",
  attack_sound = "bot_gun",
  projectile_model = "enemy_projectiles/generic_projectile",
  generic_projectile_type = "energy",
  aim_type = "any",
  projectile_sprite = "entities/enemy_projectiles/energy_bullet",
  projectile_width = 16,
  projectile_height = 16,
  damage = 2,
  type = "physical",
  recovery_delay = 1500,
}

local ranged_combo = {
  ranged_1, ranged_2, ranged_3
}


function enemy:decide_action()
  local distance = enemy:get_distance(hero)
  local rand = math.random(1, 4)
  local has_los = enemy:has_los(hero)
  if distance >= enemy.abandon_hero_distance then
    enemy:return_to_idle()
  elseif distance >= 150 and not has_los then
    local m = sol.movement.create"straight"
    m:set_max_distance(64)
    m:set_speed(75)
    m:set_angle(enemy:get_angle(hero) + math.pi / 2 * math.random(1, 2))
    m:start(enemy, function() enemy:decide_action() end)
    function m:on_obstacle_reached() enemy:decide_action() end
  elseif distance >= 100 and has_los and rand <= 2 then
    enemy:ranged_attack(ranged_attack)
  elseif distance >= 100 and has_los then
    enemy:ranged_combo(ranged_combo)
  else
    enemy:approach_then_attack({
      approach_duration = 1500,
      speed = 60,
      attack_function = function()
        if rand == 1 then
          enemy:melee_combo(slash_combo)
        elseif rand == 2 then
          enemy:melee_combo(short_combo)
        else
          enemy:melee_attack(backslash_attack)
        end
      end
    })
  end
end




function enemy:stunlock_break()
  sol.timer.stop_all(enemy)
  enemy:set_invincible()
  local m = sol.movement.create("straight")
  enemy.lock_facing = true
  local react = function()
    enemy.lock_facing = false
    enemy:ranged_attack(ranged_2)
    enemy:set_default_attack_consequences()
  end
  m:set_angle(hero:get_angle(enemy))
  m:set_speed(140)
  m:set_max_distance(48)
  m:start(enemy, react)
  m.on_obstacle_reached = react
end
