local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()

enemy.exp = 100

local TURRET_Y_OFFSET = -32

require("enemies/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = 60,
  sprite = "enemies/cybersea_enemies/uuv",
  stunlock_limit = 6,
  detection_distance = 600,
  abandon_hero_distance = 1200,
  --contact_damage = 2,
  pushed_back_when_hurt = false,
})

enemy:register_event("on_created", function()
  local bubble_x_offsets = {28, 28, -28, -28}
  local bubble_y_offsets = {-13, -44, -13, -44}
  for i = 1, 4 do
    local bubble_sprite = enemy:create_sprite"enemies/cybersea_enemies/uuv_bubble_jet"
    bubble_sprite:set_xy(bubble_x_offsets[i], bubble_y_offsets[i])
  end
  enemy.turret_sprite = enemy:create_sprite("enemies/cybersea_enemies/uuv_turret")
  enemy.turret_sprite:set_xy(0, TURRET_Y_OFFSET)
  enemy:set_size(48, 48)
  enemy:set_origin(24, 45)
  enemy:set_obstacle_behavior"flying"
  enemy:set_hurt_style"boss"
  enemy:set_pushed_back_when_hurt(false)
end)


enemy:register_event("on_restarted", function()
  enemy.is_roving = false
  sol.timer.start(enemy, 50, function()
    enemy:aim_turret()
    return enemy:get_life() > 0
  end)
end)


function enemy:aim_turret()
  enemy.turret_sprite:set_rotation(enemy:get_angle(hero))
end


local function fire_drone()
  local min_chance = 10
  local max_chance = 65
  local max = game:get_max_magic()
  local current = game:get_magic()
  if max - current < min_chance then current = max - min_chance end
  if max - current > max_chance then current = max_chance end
  local random = math.random(1, max)
  if random > current then
    sol.audio.play_sound"cane"
    enemy:create_enemy{
      direction = 0, breed = "cybersea_enemies/uuv_drone"
    }
  end
  enemy:decide_action()
end

function enemy:fire_turret(props)
  local windup_time = props.windup_time or 400
  local projectile_type = props.projectile_type or "energy"
  local callback = props.callback or enemy.decide_action
  local windup_sound = "bot_charging"
  local attack_sound = "bot_gun"
  sol.audio.play_sound(windup_sound)
  sol.timer.stop_all(enemy)
  enemy.turret_sprite:set_animation"charging"
  local direction = enemy:get_direction4_to(hero)
  enemy:aim_turret()
  shoot_angle = enemy:get_angle(hero)
  sol.timer.start(enemy, windup_time, function()
    sol.audio.play_sound(attack_sound)
    enemy.turret_sprite:set_animation("firing", "walking")
    local x, y, z = enemy:get_position()
    local projectile = map:create_custom_entity{
      x = x,
      y = y + TURRET_Y_OFFSET,
      layer = z,
      width = 16, height = 16, direction = direction, 
      model = "enemy_projectiles/generic_projectile",
      sprite = projectile_type == "missile" and "entities/enemy_projectiles/missile" or "hero_projectiles/sniper",
    }
    projectile:set_projectile_type(projectile_type)
    projectile.damage = 3
    projectile.firing_entity = enemy
    projectile:shoot(shoot_angle)
    callback()
  end)
end


function enemy:start_roving()
  enemy:stop_movement()
  local m = sol.movement.create"straight"
  m:set_angle(math.random(0,360))
  m:set_speed(75)
  m:set_max_distance(math.random(48,120))
  m:start(enemy)
  function m:on_finished()
    enemy:start_roving()
  end
  function m:on_obstacle_reached()
    enemy:start_roving()
  end
end


function enemy:decide_action()
  local distance = enemy:get_distance(hero)
  if distance >= enemy.abandon_hero_distance then
    enemy:return_to_idle()
  else
    if not enemy.is_roving then enemy:start_roving() end
    sol.timer.start(enemy, math.random(1000, 6000), function()
      enemy:random_attack()
      return math.random(2000, 6000)
    end)
  end
end


function enemy:random_attack()
  local rand = math.random(1, 10)
  if game:get_magic() < 35 then rand = rand - 3 end
  if rand <= 2 then
    enemy:drone_attack()
  elseif rand <= 5 then
    enemy:missile_attack()
  else
    enemy:laser_attack()
  end
end


function enemy:drone_attack()
  for i = 1, 4 do
    sol.timer.start(enemy, i * 100, function()
      fire_drone()
    end)
  end
end


--NOTE: the missiles just hit the enemy itself, it'll eventually just kill itself if it uses this attack
function enemy:missile_attack()
  enemy:fire_turret({ windup_time = 700, projectile_type = "missile", callback = function()
    enemy:fire_turret({ windup_time = 400, projectile_type = "missile", callback = function()
      enemy:decide_action()
    end })
  end })
end


function enemy:laser_attack()
  enemy:fire_turret({ windup_time = 500, callback = function()
    enemy:fire_turret({ windup_time = 200, callback = function()
      enemy:fire_turret({ windup_time = 200, callback = function()
        enemy:decide_action()
      end })
    end })
  end })
end


function enemy:stunlock_break()
  enemy:stop_movement()
  local m = sol.movement.create"straight"
  m:set_angle(hero:get_angle(enemy))
  m:set_speed(180)
  m:set_smooth(true)
  m:start(enemy)
  enemy:laser_attack()
end
