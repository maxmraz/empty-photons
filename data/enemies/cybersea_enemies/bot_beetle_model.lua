local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()

enemy.exp = 0

require("enemies/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = 2,
  detection_distance = 96,
  abandon_hero_distance = 150,
})
require("enemies/trillium_enemies/attacks/ram_attack"):apply_behavior(enemy)

local ram_attack = {
  damage = 2,
  windup_animation = "rising",
  windup_sound = "bot_piston",
  attack_animation = "spinning",
  attack_sound = "bot_spin_attack",
  weapon_sprite = "enemies/cybersea_enemies/bot_beetle_model",
  weapon_windup_animation = "invisible",
  weapon_attack_animation = "spin_effect",
  max_distance = 32,
  speed = 90,
  recovery_animation = "landing",
}


enemy:register_event("on_restarted", function()
  enemy:set_traversable(true)
end)


function enemy:decide_action()
  local distance = enemy:get_distance(hero)
  if (distance >= enemy.abandon_hero_distance) then
    enemy:return_to_idle()
  elseif not enemy.ram_cooldown then
    enemy:ram_attack(ram_attack)
    enemy.ram_cooldown = true
    sol.timer.start(map, 2000, function()
      enemy.ram_cooldown = false
    end)
  else
    enemy:move_randomly()
  end
end


function enemy:move_randomly()
  local sprite = enemy:get_sprite()
  sprite:set_animation"walking"
  local m = sol.movement.create"straight"
  m:set_max_distance(math.random(16, 40))
  m:set_speed(80)
  m:set_angle(math.rad(math.random(0, 360)))
  m:start(enemy, function()
    sprite:set_animation"stopped"
    enemy:decide_action()
  end)
  function m:on_obstacle_reached()
    sprite:set_animation"stopped"
    enemy:decide_action()
  end
end

