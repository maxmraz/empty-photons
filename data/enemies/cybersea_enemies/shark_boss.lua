local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()

enemy.exp = 150

require("enemies/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life =130,
  detection_distance = 400,
  abandon_hero_distance = 700,
  stunlock_limit = 6,
  stunlock_reset_rate = 800,
})
require("enemies/trillium_enemies/attacks/melee"):apply_melee(enemy)
require("enemies/trillium_enemies/attacks/ranged"):apply_ranged(enemy)
require("enemies/trillium_enemies/attacks/ram_attack"):apply_behavior(enemy)


enemy:register_event("on_created", function()
  enemy:set_hurt_style"boss"
end)


local fist_attack = {
  windup_animation = "punch_windup",
  windup_time = 1000,
  attack_animation = "punch_attack",
  attack_sound = "bot_gun",
  projectile_model = "enemy_projectiles/generic_projectile",
  generic_projectile_type = "energy",
  aim_type = "any",
  projectile_sprite = "enemies/cybersea_enemies/shark_boss_fist",
  projectile_width = 16,
  projectile_height = 16,
  damage = 4,
  type = "physical",
  recovery_delay = 1000,
  recovery_animation = "punch_recovery",
}

local ranged_1 = {
  windup_animation = "finger_missile",
  windup_sound = "bot_charge",
  windup_time = 1000,
  attack_animation = "finger_missile_fire",
  attack_sound = "bot_gun",
  projectile_model = "enemy_projectiles/generic_projectile",
  generic_projectile_type = "missile",
  aim_type = "any",
  projectile_sprite = "entities/enemy_projectiles/missile",
  projectile_width = 16,
  projectile_height = 16,
  damage = 4,
  type = "physical",
  recovery_delay = 200,
}

local ranged_2 = {
  windup_animation = "finger_missile",
  windup_time = 50,
  attack_animation = "finger_missile_fire",
  attack_sound = "bot_gun",
  projectile_model = "enemy_projectiles/generic_projectile",
  generic_projectile_type = "missile",
  aim_type = "any",
  projectile_sprite = "entities/enemy_projectiles/missile",
  projectile_width = 16,
  projectile_height = 16,
  damage = 4,
  type = "physical",
  recovery_delay = 200,
}

local ranged_3 = {
  windup_animation = "finger_missile",
  windup_time = 50,
  attack_animation = "finger_missile_fire",
  attack_sound = "bot_gun",
  projectile_model = "enemy_projectiles/generic_projectile",
  generic_projectile_type = "missile",
  aim_type = "any",
  projectile_sprite = "entities/enemy_projectiles/missile",
  projectile_width = 16,
  projectile_height = 16,
  damage = 4,
  type = "physical",
  recovery_delay = 1500,
}

local missile_combo = {
  ranged_1, ranged_2, ranged_3
}



local ram_attack = {
  windup_duration = 700,
  windup_animation = "ram_windup",
  speed = 220,
  damage = 3,
  attack_sound = "bubbles_dash",
  attack_animation = "dive_roll",
  weapon_sprite = "enemies/cybersea_enemies/shark_boss",
  weapon_windup_animation = "invisible",
  weapon_attack_animation = "dive_hitbox",
  max_distance = 256,
}

local quick_ram = {
  windup_duration = 100,
  windup_animation = "ram_windup",
  speed = 220,
  attack_sound = "bubbles_dash",
  attack_animation = "dive_roll",
  weapon_sprite = "enemies/cybersea_enemies/shark_boss",
  weapon_windup_animation = "invisible",
  weapon_attack_animation = "dive_hitbox",
  max_distance = 256,
  next_action = function() enemy:ranged_combo(missile_combo) end,
}



function enemy:decide_action()
  local melee_distance = 80
  local distance = enemy:get_distance(hero)
  local has_los = enemy:has_los(hero)
  if distance >= enemy.abandon_hero_distance then
    enemy:return_to_idle()
  elseif distance >= melee_distance and not has_los then
    enemy:approach_then_attack({
      approach_duration = 1500,
      speed = 60,
      attack_function = function()
        enemy:ranged_attack(fist_attack)
      end
    })
  elseif distance >= melee_distance and has_los then
    if math.random(1, 5) >= 3 then
      enemy:ranged_combo(missile_combo)
    else
      enemy:ranged_attack(fist_attack)
    end
  else
    enemy:ram_attack(ram_attack)
  end
end


enemy:register_event("on_restarted", function()
  enemy:set_attack_consequence("explosion", function()
    enemy:stun()
  end)
end)


function enemy:stun(stun_duration)
  sol.timer.stop_all(enemy)
  enemy:stop_movement()
  local sprite = enemy:get_sprite()
  sprite:set_animation"stunned"
  local old_hurt_reaction = enemy.process_hit
  enemy.process_hit = function(attack_power)
    sol.audio.play_sound"enemy_hurt"
    sprite:set_animation"hurt_stunned"
    sol.timer.start(enemy:get_map(), 150, function()
      sprite:set_animation"stunned"
    end)
    if enemy:get_life() > 0 then
      enemy:remove_life(4)
    end
  end
  sol.timer.start(enemy, stun_duration or 3000, function()
    enemy.process_hit = enemy.old_hurt_reaction
    enemy:restart()
  end)
end




function enemy:stunlock_break()
  enemy:set_invincible()
  sol.timer.stop_all(enemy)
  enemy:ram_attack(quick_ram)
  sol.timer.start(map, 400, function()
    enemy:set_default_attack_consequences()
    enemy:set_attack_consequence("explosion", function()
      enemy:stun()
    end)
  end)
end
