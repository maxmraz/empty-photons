local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()

enemy.exp = 5

require("enemies/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = 6,
  detection_distance = 200,
  abandon_hero_distance = 400,
})
require("enemies/trillium_enemies/attacks/melee"):apply_melee(enemy)


local slash_attack = {
  windup_animation = "slash_windup",
  windup_delay = 500,
  attack_animation = "slash_attack",
  attack_sound = "swish_1",
  weapon_sprite = "enemies/trillium_enemies/weapons/hob_sword",
  weapon_windup_animation = "hidden",
  weapon_attack_animation = "slash_attack",
  damage = 2,
  type = "physical",
  attack_movement_distance = 32,
  attack_movement_speed = 200,
  recovery_delay = 800,
}



function enemy:decide_action()
  local distance = enemy:get_distance(hero)
  if distance >= enemy.abandon_hero_distance then
    enemy:return_to_idle()
  elseif distance >= 80 then
    --Too far from hero, approach
    local m = sol.movement.create"target"
    m:start(enemy)
    sol.timer.start(enemy, 400, function() enemy:decide_action() end)
  else
    --close enough, decide on an action
    local rand = math.random(1, 5)
    if rand <= 2 or (distance < 40) then
      enemy:approach_then_attack({
        approach_duration = 500,
        speed = 60,
        attack_function = function()
          enemy:melee_attack(slash_attack)
        end
      })
    elseif rand == 3 then
      --pause
      enemy:stop_movement()
      if enemy:get_sprite():has_animation"stopped" then
        enemy:get_sprite():set_animation("stopped")
      end
      sol.timer.start(enemy, math.random(100,600), function() enemy:decide_action() end)
    else
      --back off, move randomly
      local m = sol.movement.create"straight"
      m:set_angle(math.rad(math.random(0,360)))
      m:set_max_distance(math.random(16, 48))
      m:start(enemy, function() enemy:restart() end)
      function m:on_obstacle_reached() enemy:restart() end
    end
  end
end
