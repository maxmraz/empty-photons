local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()

enemy.exp = 10
enemy.bullet_damage_mod = 2

require("enemies/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = 8,
  detection_distance = 200,
  abandon_hero_distance = 400,
})
require("enemies/trillium_enemies/attacks/melee"):apply_melee(enemy)
require("enemies/trillium_enemies/attacks/ranged"):apply_ranged(enemy)

enemy:register_event("on_created", function()
  enemy:set_pushed_back_when_hurt(false)
  --enemy:set_traversable(false)
  local x, y, z = enemy:get_position()
  local blocker = map:create_custom_entity{
    x=x, y=y, layer=z,
    width = 16, height = 16, direction = 0,
  }
  blocker:set_traversable_by("hero", false)
  enemy:register_event("on_dying", function()
    blocker:remove()
  end)
end)

local ranged_attack = {
  damage = 4,
  windup_animation = "aiming",
  windup_sound = "bot_charging",
  windup_time = 800,
  attack_animation = "shooting",
  attack_sound = "bot_laser",
  projectile_model = "enemy_projectiles/generic_projectile",
  generic_projectile_type = "energy",
  aim_type = "any",
  projectile_sprite = "entities/enemy_projectiles/energy_bullet",
  projectile_width = 16,
  projectile_height = 16,
  damage = 2,
  type = "physical",
  recovery_delay = 1000,
}

local ranged_attack1 = {
  damage = 4,
  windup_animation = "aiming",
  windup_sound = "bot_charging",
  windup_time = 800,
  attack_animation = "shooting",
  attack_sound = "bot_laser",
  projectile_model = "enemy_projectiles/generic_projectile",
  generic_projectile_type = "energy",
  aim_type = "any",
  projectile_sprite = "entities/enemy_projectiles/energy_bullet",
  projectile_width = 16,
  projectile_height = 16,
  damage = 2,
  type = "physical",
  recovery_delay = 10,
}
local ranged_attack2 = {
  damage = 4,
  windup_animation = "aiming",
  windup_time = 200,
  attack_animation = "shooting",
  attack_sound = "bot_laser",
  projectile_model = "enemy_projectiles/generic_projectile",
  generic_projectile_type = "energy",
  aim_type = "any",
  projectile_sprite = "entities/enemy_projectiles/energy_bullet",
  projectile_width = 16,
  projectile_height = 16,
  damage = 2,
  type = "physical",
  recovery_delay = 1500,
}



local ranged_combo = {ranged_attack1, ranged_attack2,}

function enemy:decide_action()
  local distance = enemy:get_distance(hero)
  if distance >= enemy.abandon_hero_distance then
    enemy:return_to_idle()
  elseif enemy:has_los(hero) then
    sol.timer.start(enemy, math.random(10,150), function()
      if math.random(1, 3) == 1 then
        enemy:ranged_combo(ranged_combo)
      else
        enemy:ranged_attack(ranged_attack)
      end
    end)
  else
    enemy:be_watchful()
  end
end


function enemy:be_watchful()
  enemy:get_sprite():set_direction(enemy:get_direction4_to(hero))
  sol.timer.start(enemy, 100, function()
    enemy:decide_action()
  end)
end
