local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()

enemy.exp = 0

require("enemies/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = 50,
  detection_distance = 300,
  abandon_hero_distance = 900,
  --contact_damage = 2,
  pushed_back_when_hurt = false,
})

enemy:register_event("on_created", function()
  local bubble_x_offsets = {28, 28, -28, -28}
  local bubble_y_offsets = {-13, -44, -13, -44}
  for i = 1, 4 do
    local bubble_sprite = enemy:create_sprite"enemies/cybersea_enemies/uuv_bubble_jet"
    bubble_sprite:set_xy(bubble_x_offsets[i], bubble_y_offsets[i])
  end
  enemy:set_size(32, 48)
  enemy:set_origin(16, 45)
  enemy:set_obstacle_behavior"flying"
  enemy:set_hurt_style"boss"
  enemy:set_pushed_back_when_hurt(false)
end)


enemy:register_event("on_restarted", function()
end)


local function fire_drone()
  local min_chance = 10
  local max_chance = 65
  local max = game:get_max_magic()
  local current = game:get_magic()
  if max - current < min_chance then current = max - min_chance end
  if max - current > max_chance then current = max_chance end
  local random = math.random(1, max)
  if random > current then
    sol.audio.play_sound"cane"
    enemy:create_enemy{
      direction = 0, breed = "cybersea_enemies/uuv_drone"
    }
  end
  enemy:decide_action()
end

function enemy:fire_turret()
  enemy:stop_movement()
  local x, y, z = enemy:get_position()
end


function enemy:decide_action()
  local distance = enemy:get_distance(hero)
  if distance >= enemy.abandon_hero_distance then
    enemy:return_to_idle()
  else
    local m = sol.movement.create"straight"
    m:set_angle(math.random(0,360))
    m:set_speed(75)
    m:set_max_distance(math.random(48,120))
    m:start(enemy)
    function m:on_finished()
      fire_drone()
    end
    function m:on_obstacle_reached()
      fire_drone()
    end
  end
end
