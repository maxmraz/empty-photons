local manager = {}

function manager:apply_behavior(enemy)
  local game = enemy:get_game()
  local map = enemy:get_map()
  local hero = map:get_hero()
  local sprite = enemy:get_sprite()

  function enemy:spoke_bullet_attack(props)
    props = props or {}
    local num_projectiles = props.num_projectiles or 6
    local windup_animation = props.windup_animation or "walking"
    local windup_time = props.windup_time or 500
    local attack_animation = props.attack_animation
    local attack_sound = props.attack_sound
    local projectile_model = props.projectile_model or "enemy_projectiles/generic_projectile"
    local projectile_sprite = props.projectile_sprite or "entities/enemy_projectiles/generic_projectile"
    local projectile_speed = props.projectile_speed or 150
    local max_distance = props.max_distance or 700
    local rotational_sprite = props.rotational_sprite or false
    local generic_projectile_type = props.generic_projectile_type
    local damage = props.damage or 1
    local offset_radius = props.offset_radius or 0 --how far from the origin the projectiles are created (likely needs to be more than the width/height of the enemy)
    local origin_offset = props.origin_offset or {x = 0, y = 0}
    local recovery_animation = props.recovery_animation or "walking"
    local recovery_time = props.recovery_time or 500
    local callback = props.callback or function() enemy:decide_action() end

    local sprite = enemy:get_sprite()
    sprite:set_animation(windup_animation)
    sol.timer.start(enemy, windup_time, function()
      sprite:set_animation(attack_animation, recovery_animation)
      if attack_sound then sol.audio.play_sound(attack_sound) end
      local x, y, z = enemy:get_position()
      for i = 1, num_projectiles do
        local angle = math.pi * 2 / num_projectiles * (i - 1)
        local projectile = map:create_custom_entity{
          x = x + origin_offset.x + offset_radius * math.cos(angle),
          y = y + origin_offset.y + offset_radius * math.sin(angle) * -1,
          layer = z,
          width = 8, height = 8, direction = 0,
          model = projectile_model, sprite = projectile_sprite,
        }
        projectile.damage = damage
        if generic_projectile_type then
          projectile:set_projectile_type(generic_projectile_type)
        end
        projectile.max_distance = max_distance
        projectile.speed = projectile_speed
        projectile.rotational_sprite = rotational_sprite
        projectile.firing_entity = enemy
        projectile:shoot(angle)
      end
      sol.timer.start(enemy, recovery_time, function()
        callback()
      end)
    end)

  end

end

return manager
