local manager = {}

function manager:apply_melee(enemy)
  local game = enemy:get_game()
  local map = enemy:get_map()
  local hero = map:get_hero()

  function enemy:melee_attack(props)
    local sprite = enemy:get_sprite()
    local windup_delay = props.windup_delay or 500
    local windup_animation = props.windup_animation
    local attack_animation = props.attack_animation
    local weapon_sprite = props.weapon_sprite
    local weapon_windup_animation = props.weapon_windup_animation
    local weapon_attack_animation = props.weapon_attack_animation
    local attack_sound = props.attack_sound or "sword1"
    local damage = props.damage or 1
    local type = props.type or "physical"
    local has_aoe = props.has_aoe or false
    local attack_movement_distance = props.attack_movement_distance
    local attack_movement_speed = props.attack_movement_speed or 90
    local recovery_delay = props.recovery_delay or 500
    local next_action = props.next_action or (function() enemy:decide_action() end)


    enemy:stop_movement()
    local x, y, z = enemy:get_position()
    local direction = enemy:get_direction4_to(hero)
    sprite:set_direction(direction)
    sprite:set_animation(windup_animation)
    local attack_entity = map:create_custom_entity{
      x = x, y = y, layer = z, direction = direction,
      width = 16, height = 16,
      name = "enemy_melee_attack",
      sprite = weapon_sprite,
    }
    enemy.attack_entities[attack_entity] = attack_entity
    local attack_sprite = attack_entity:get_sprite()
    attack_sprite:set_animation(weapon_windup_animation)
    attack_sprite:set_direction(direction)

    sol.timer.start(enemy, windup_delay, function()
      sol.audio.play_sound(attack_sound)
      sprite:set_animation(attack_animation, "stopped")
      attack_sprite:set_animation(weapon_attack_animation)
      attack_entity:add_collision_test("sprite", function(attack_entity, other)
        if other:get_type() == "hero" and hero:get_can_be_hurt() then
          attack_entity:clear_collision_tests()
          if hero.process_hit then
            hero:process_hit({damage = damage, enemy = enemy, type = type})
          else
            hero:start_hurt(enemy, damage)
          end
        end
      end)
      if has_aoe then
        enemy:create_melee_aoe(props)
      end
      local attack_duration = sprite:get_num_frames(attack_animation, direction) * sprite:get_frame_delay(attack_animation)
      sol.timer.start(enemy, attack_duration + recovery_delay, function()
        enemy.attack_entities[attack_entity]:remove()
        next_action()
      end)
      if attack_movement_distance then
        local m = sol.movement.create"straight"
        m:set_angle(sprite:get_direction() * math.pi / 2)
        m:set_speed(attack_movement_speed)
        m:set_max_distance(attack_movement_distance)
        m:start(enemy)
      end
    end)
  end


  function enemy:create_melee_aoe(props)
    local aoe_offset = props.aoe_offset or 0
    local aoe_delay = props.aoe_delay or 200
    local aoe_sprite = props.aoe_sprite or "enemies/trillium_enemies/shockwave_8x7"
    local aoe_damage = props.aoe_damage or 1
    local aoe_sound = props.aoe_sound
    local aoe_screenshake = props.aoe_screenshake
    local aoe_scale = props.aoe_scale or {1, 1}

    local x, y, z = enemy:get_position()
    local direction = enemy:get_sprite():get_direction()
    sol.timer.start(map, aoe_delay, function()
      if aoe_sound then sol.audio.play_sound(aoe_sound) end
      if aoe_screenshake then map:screenshake({shake_count = aoe_screenshake}) end
      local shockwave = map:create_custom_entity{
        x = x + game:dx(aoe_offset)[direction], y = y + game:dy(aoe_offset)[direction], layer = z,
        direction = 0, width = 16, height = 16,
        sprite = aoe_sprite,
      }
      if aoe_scale then shockwave:get_sprite():set_scale(aoe_scale[1], aoe_scale[2]) end
      shockwave:add_collision_test("sprite", function(shockwave, other)
        if (other:get_type() == "hero") and hero:get_can_be_hurt() then
          shockwave:clear_collision_tests()
          hero:start_hurt(aoe_damage)
        end
      end)
    end)
  end



  function enemy:melee_combo(attacks)
    for i = 1, #attacks - 1 do
      attacks[i].next_action = function()
        enemy:melee_attack(attacks[i + 1])
      end
    end
    enemy:melee_attack(attacks[1])
  end


  enemy:register_event("on_position_changed", function(self, x, y, z)
    for attack in pairs(enemy.attack_entities) do
      attack:set_position(x, y, z)
    end
  end)


end


return manager