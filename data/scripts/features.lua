-- Sets up all non built-in gameplay features specific to this quest.

-- Usage: require("scripts/features")

-- Features can be enabled to disabled independently by commenting
-- or uncommenting lines below.

require"scripts/multi_events"

--require"scripts/action/swim_manager"
require"scripts/coroutine_helper"
require"scripts/dialogs/visual_novel_manager"
require"scripts/detective/clue_manager"
require"scripts/elements/enemy_elemental_meta"
require"scripts/elements/hero_elemental_meta"
require"scripts/elements/map_elemental_meta"
require"scripts/fx/fog"
require"scripts/fx/lighting/lighting_manager"
require"scripts/fx/lighting/map_lighting"
require"scripts/fx/white_flash"
require"scripts/gameover"
require"scripts/hud/hud"
--require"scripts/menus/dialog_box" --replaced with VNS
require"scripts/meta/bush"
require"scripts/meta/camera"
require"scripts/meta/custom_entity"
require"scripts/meta/enemy"
require"scripts/meta/game"
require"scripts/meta/hero"
require"scripts/meta/map"
require"scripts/meta/switch"
--require"scripts/misc/solid_ground_manager"
require"scripts/menus/pause_menu/options"
require"scripts/status_effects/status_manager"
require"scripts/utility/draw_bounding_boxes"
require("scripts/utility/ammo_tracker").add_to_item_api()
require"scripts/utility/item_name_retriever"
require"scripts/utility/savegame_tables"
require"scripts/weather/weather_manager"

return true
