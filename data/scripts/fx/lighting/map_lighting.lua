--[[
Lighting system by Max Mraz. Licensed MIT
This is meant to be used with the script at scripts/fx/lighting/lighting manager. Both should be required when the program or game is started
This script automatically creates lighting effects around certain entities:

### Static Light Sources:
These light sources must exist at the map's creation, and cannot move or be removed
- Any entity with a name starting with "^sprite_light_source" will have its sprite applied as a light source.
- And entity with a name starting with "^lighting_effect" will be checked for keywords in its name:
  - "candle" : will produce a small circle of light
  - "torch" : will produce a medium circle of light

### Dynamic Light Sources:
The map will check for entities that meet a certain criteria, and if they're close enough to the camera, will apply a light source sprite to them
See scripts/fx/lighting/dynamic_light_source_sprite_manager for details
the function `manager:create_light_source()` from that script takes an entity, and returns an table with the values
- `entity` the entity passed to the function
- `sprite` a sprite object to be drawn at the entity's coordinates
Note: the function can use `lighting_manager:get_effect_sprites()` to just redraw a few preset sprites, rather than creating new sprites.
--]]

local lighting_manager = require"scripts/fx/lighting/lighting_manager"
local dynamic_light_source_sprite_manager = require"scripts/fx/lighting/dynamic_light_source_sprite_manager"
local map_meta = sol.main.get_metatable"map"

function map_meta:set_darkness_level(level)
  local map = self
  if not sol.menu.is_started(lighting_manager) then
    sol.menu.start(self, lighting_manager)
  end
  lighting_manager:set_darkness_level(level)

  --Add static light sources
  map.static_light_sources = {}
  local effect_sprites = lighting_manager:get_effect_sprites()

  local function add_static_light_source(entity)
    local x, y, z = entity:get_position()
    local sprite = entity:get_sprite()
    local source = {
      sprite = sprite,
      x = x,
      y = y,
    }
    table.insert(map.static_light_sources, source)
    if not sprite:get_animation_set():match("sign") then
      entity:set_visible(false)
    end
  end

  for entity in map:get_entities("^sprite_light_source") do
    if entity:is_enabled() then
      add_static_light_source(entity)
    end
  end

  for entity in map:get_entities("^lighting_effect") do
    if entity:is_enabled() then
      local name = entity:get_name()
      local x, y, z = entity:get_position()
      local source = {x=x, y=y}
      if string.match(name, "torch") then
        source.sprite = effect_sprites.torch
      elseif string.match(name, "candle") then
        source.sprite = effect_sprites.candle
      end
      table.insert(map.static_light_sources, source)
    end
  end

  --check for dynamic light sources regularly
  local width, height = 600, 400
  local camera = map:get_camera()
-- think about removing this for now, it might be causing really bad performance issues
  sol.timer.start(map, 50, function()
    map.dynamic_light_sources = {}
    local camx, camy = camera:get_center_position()
    for entity in map:get_entities_in_rectangle(camx - (width/2), camy - (height/2), width, height) do
      if not entity:is_enabled() then break end
      if not entity:is_visible() then break end
      if not entity:exists() then break end
      local source = dynamic_light_source_sprite_manager:create_light_source(entity)
      if source.sprite then table.insert(map.dynamic_light_sources, source) end
    end
    return true
  end)
--]]

end


function map_meta:get_darkness_level()
  return lighting_manager:get_darkness_level()
end


function map_meta:update_lighting()
  self:set_darkness_level(lighting_manager:get_darkness_level())
end