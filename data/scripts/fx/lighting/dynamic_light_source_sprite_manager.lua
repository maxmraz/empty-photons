local manager = {}

local lighting_manager = require"scripts/fx/lighting/lighting_manager"
local effect_sprites = lighting_manager:get_effect_sprites()

function manager:create_light_source(entity)
  local entity_type = entity:get_type()
  local source = {entity = entity}
  if entity.light_source_sprite then
    local entity_sprite = entity:get_sprite()
    source.sprite = sol.sprite.create(entity.light_source_sprite)
    source.sprite:set_xy(entity_sprite:get_xy())
    source.sprite:set_transformation_origin(entity_sprite:get_transformation_origin())
    if entity.light_source_sprite_animation_matched then
      source.sprite:set_direction(entity_sprite:get_direction())
      source.sprite:set_animation(entity_sprite:get_animation())
      source.sprite:set_frame(entity_sprite:get_frame())
    end
  elseif entity_type == "explosion" then
    source.sprite = effect_sprites.explosion
  elseif entity_type == "fire" then
    source.sprite = effect_sprites.torch
  elseif entity_type == "custom_entity" then
    local model = entity:get_model()
    if string.match(model, "elements/flame") then
      source.sprite = effect_sprites.torch
    elseif string.match(model, "elements/lightning") then
      source.sprite = effect_sprites.torch
    elseif string.match(model, "elements/smolder") then
      source.sprite = effect_sprites.candle
    elseif string.match(model, "breakables/light") then
      source.sprite = effect_sprites.candle
    end
  elseif entity_type == "enemy" then
    if entity.lighting_effect == 1 then
      source.sprite = effect_sprites.candle
    elseif entity.lighting_effect then
      source.sprite = effect_sprites.torch
    end
  end

  return source
end

return manager