--[[
By Max Mraz, licensed MIT

Necessary resources:
sprites/status_effects/status_effect
- sprite, if you want a sprite to play on the hero when they're affected by a status effect, it must have an animation here

game:get_status_effects() returns a table of all status effects that have been activated (ones that are not currently active will be nil)
That table would look something like this:
game:get_status_effects() ->
{
  poison = {
    timer = [a timer object],
    type = "poison",
  },
  swim_speed_boost = {
    timer = [a timer object],
    type = "swim_speed_boost",
  },
}

Note: status_effects timers do NOT show total remaining time, they loop for a duration
--]]

local manager = {}
local game_meta = sol.main.get_metatable"game"
local status_effects = {}
local status_immunities = {}
local status_effect_reference_sprite = sol.sprite.create"status_effects/status_effect"


function game_meta:get_status_effects()
  return status_effects
end


function game_meta:get_status_immunities()
  return status_immunities
end


--All effects must be processed here. Duration can be:
--a number (milliseconds), "map" (effect lasts while on map), or "infinite" (effect lasts until game:stop_status_effect() is called)
function game_meta:start_status_effect(effect, duration)
  local game = self
  local hero = game:get_hero()
  if status_immunities[effect] then
    return
  elseif game:is_status_effect_active(effect) then
    return
  end
  status_effects[effect] = {}
  status_effects[effect].type = effect
  if effect == "poison" then
    manager:start_dps(effect, 1, 2000, duration or 16000)

  elseif effect == "acid" then
    manager:start_dps(effect, 1, 500, duration or 5000)

  elseif effect == "burn" then
    manager:start_dps(effect, 1, 1000, duration or 5000)

  elseif effect == "cold" then
    manager:start_dps(effect, 1, 2000, "map")
    status_effects[effect].map_id = game:get_map():get_id()

  elseif effect == "sunstroke" then
    manager:start_dps(effect, 1, 3000, "map")
    status_effects[effect].map_id = game:get_map():get_id()

  elseif effect == "hex" then
    game:set_magic(0)
    game:set_magic_regen_multiplier(nil, 0)
    status_effects[effect].timer = sol.timer.start(game, duration or 60000, function()
      game:set_magic_regen_multiplier(nil, 1)
      game:stop_status_effect(effect)
    end)

  elseif effect == "frozen" then
    hero:start_state(manager:get_frozen_state())
    if hero:get_sprite():has_animation("frozen") then
      hero:set_animation("frozen")
    end
    status_effects[effect].timer = sol.timer.start(game, duration or 3000, function()
      hero:set_animation"stopped"
      hero:unfreeze()
      game:stop_status_effect(effect)
    end)

  elseif effect == "webbed" then
    hero:start_state(manager:get_frozen_state())
    if hero:get_sprite():has_animation("webbed") then
      hero:set_animation("webbed")
    end
    status_effects[effect].timer = sol.timer.start(game, duration or 2000, function()
      hero:set_animation"stopped"
      hero:unfreeze()
      game:stop_status_effect(effect)
    end)

  else
    error("nonexistant status effect called with game:start_status_effect()")
  end
  if status_effect_reference_sprite:has_animation(effect) then
    game:start_status_sprite(effect)
  end
end



function game_meta:stop_status_effect(effect)
  local game = self
  local hero = game:get_hero()
  if status_effects[effect] then
    status_effects[effect].timer:stop()
    status_effects[effect] = nil
  end
  local effect_sprite = hero:get_sprite("status_effect_" .. effect)
  if effect_sprite then hero:remove_sprite(effect_sprite) end
end


function game_meta:is_status_effect_active(effect)
  local game = self
  local is_active = false
  for _, v in pairs(status_effects) do
    if v.type == effect then is_active = true end
  end
  return is_active
end


function game_meta:start_status_sprite(effect)
  local game = self
  local hero = game:get_hero()
  local sprite = hero:create_sprite("status_effects/status_effect", "status_effect_" .. effect)
  sprite:set_animation(effect)
end


function game_meta:cure_all_status_effects()
  local game = self
  for _, effect in pairs(status_effects) do
    game:stop_status_effect(effect.type)
  end
end


function game_meta:start_status_immunity(effect, duration)
  local game = self
  if status_immunities[effect] then status_immunities[effect].timer:stop() end
  status_immunities[effect] = {}
  status_immunities[effect].type = effect
  status_immunities[effect].timer = sol.timer.start(game, duration, function()
    status_immunities[effect] = nil
  end)
  game:stop_status_effect(effect)
end



function manager:start_dps(effect, amount, rate, duration)
  local game = sol.main.get_game()
  local elapsed_time = 0
  --if duration is "map", set a timer to check that we're still on the same map
  if duration == "map" then
    sol.timer.start(game, 500, function()
      if status_effects[effect] and status_effects[effect].map_id == game:get_map():get_id() then
        return true
      else
        game:stop_status_effect(effect)
      end
    end)
  end
  --Start a timer to do damage
  status_effects[effect].timer = sol.timer.start(game, rate, function()
    elapsed_time = elapsed_time + rate
    game:remove_life(amount)
    sol.audio.play_sound"hero_hurt"
    if (duration == "infinite") or (duration == "map") then
      return true
    elseif (elapsed_time < duration) then
      return true
    else
      game:stop_status_effect(effect)
    end
  end)
  status_effects[effect].timer:set_suspended_with_map(true)
end


function manager:get_frozen_state()
  local state = sol.state.create()
  state:set_can_control_direction(false)
  state:set_can_control_movement(false)
  state:set_can_use_sword(false)
  state:set_can_use_item(false)
  state:set_can_interact(false)
  state:set_can_grab(false)
  state:set_can_push(false)
  state:set_can_pick_treasure(false)
  return state
end



return manager