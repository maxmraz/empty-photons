-- This script initializes game values for a new savegame file.
-- You should modify the initialize_new_savegame() function below
-- to set values like the initial life and equipment
-- as well as the starting location.
--
-- Usage:
-- local initial_game = require("scripts/initial_game")
-- initial_game:initialize_new_savegame(game)

local initial_game = {}

-- Sets initial values to a new savegame file.
function initial_game:initialize_new_savegame(game)

  sol.audio.set_sound_volume(50)
  sol.audio.set_music_volume(50)

  game:set_starting_location("example/intro", "start")  -- Starting location.
  if sol.main.debug_mode then    
    game:set_starting_location("debug", "destination")  -- Starting location.
  end

  game:set_max_life(10)
  game:set_life(game:get_max_life())
  game:set_max_money(1000000)
  game:set_money(50)
  game:set_max_magic(100)
  game:set_magic(100)
  game:set_ability("lift", 0)
  game:set_ability("push", 0)
  game:set_ability("grab", 0)
  game:set_ability("pull", 0)
  game:set_ability("sword", 0)
  game:set_ability("swim", 1)
  game:set_value("equipped_weapon", "solforge/basic_sword")
  game:get_item("weapons/pistol"):set_variant(1)
  game:set_value("sword_magic_regen_amount", 15)
  game:set_value("grenade_ammo_cost", 40)
  game:set_value("charge_heal_amount", 3)
  game:set_value("charge_heal_cost", 50)
  game:set_value("charge_heal_time", 1000)
  game:set_value("pistol_ammo_cost_multiplier", 100) --the percentage of normal ammo cost actually used
  game:set_value("shotgun_ammo_cost_multiplier", 100)
  game:set_value("ability_can_deflect_projectiles", true) --start out with deflect, it wasted too much money to buy and is super fun

  local heal = game:get_item("abilities/heal")
  --heal = game:get_item("weapons/grenade")
  heal:set_variant(1)
  game:set_item_assigned(2, heal)

  local gun = game:get_item("weapons/gun")
  gun:set_variant(1)
  game:set_item_assigned(1, gun)

  --Options
  game:set_value("show_clue_shines", true)

end

return initial_game
