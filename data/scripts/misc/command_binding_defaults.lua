--[[
Switch Buttons
A, B, X, Y -> 4, 5, 6, 7
L, R -> 8, 9
+, - -> 14, 15
--]]

local keyboard = {
  up = "w",
  down = "s",
  left = "a",
  right = "d",
  action = "return",
  attack = "right shift",
  item_1 = "q",
  item_2 = "e",
  pause = "escape",
--custom_commands
  grenade = "space",
  fire = "left shift",
  switch_gun = "/",
}

local joypad
if sol.main.get_os() == "Nintendo Switch" then
  joypad = {
    up = "axis 1 -",
    down = "axis 1 +",
    left = "axis 0 -",
    right = "axis 0 +",
    action = "button 4",
    attack = "button 5",
    pause = "button 14",
    item_1 = "button 10",
    item_2 = "button 7",
    --custom_commands
    grenade = "button 9",
    fire = "button 11",
    switch_gun = "button 8",
  }
else
  joypad = {
    up = "axis 1 -",
    down = "axis 1 +",
    left = "axis 0 -",
    right = "axis 0 +",
    action = "button 0",
    attack = "button 5",
    pause = "button 7",
    item_1 = "button 2",
    item_2 = "button 3",
    --custom_commands
    grenade = "button 4",
    fire = "button 1",
    switch_gun = "axis 5 +",
  }
end

local keyboard_solarus = {
  up = "up",
  down = "down",
  left = "left",
  right = "right",
  action = "return",
  attack = "c",
  pause = "d",
  item_1 = "x",
  item_2 = "v",
  --custom commands
  grenade = "z",
  fire = "space",
  switch_gun = "left shift",
}

return {
  ["keyboard"] = keyboard_solarus,
  ["joypad"] = joypad,
}

