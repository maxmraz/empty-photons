local manager = {}
local rate = 500
local rate_multiplier = 1
local amount = 1
local amount_multiplier = 1

function manager:init(game)
  local timer = sol.timer.start(game, rate / rate_multiplier, function()
    game:add_magic(amount * amount_multiplier)
    return rate / (rate_multiplier or 1)
  end)
  timer:set_suspended_with_map(true)

  function game:set_magic_regen_multiplier(rm, am)
    rate_multiplier = rm or rate_multiplier
    amount_multiplier = am or amount_multiplier
  end

  function game:get_magic_regen_multiplier()
    return rate_multiplier, amount_multiplier
  end

end

return manager