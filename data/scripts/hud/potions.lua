local builder = {}

local CHECK_FREQUENCY = 500

function builder:new(game, config)
  local item = game:get_item("equipment/potion_flask")
  local element = {}
  element.dst_x, element.dst_y = config.x, config.y
  element.surface = sol.surface.create(64, 64)
  element.background = sol.surface.create("hud/potions.png")
  element.numbers_surface = sol.text_surface.create{
    font = "white_digits",
    horizontal_alignment = "left",
    vertical_alignment = "top",
  }
  element.amount_displayed = 0

  function element:check()
    local real_amount = item:get_amount()
    if real_amount ~= element.amount_displayed then
      element:rebuild_surface()
    end
    sol.timer.start(element, CHECK_FREQUENCY, function() element:check() end)
  end

  function element:rebuild_surface()
    local amount = item:get_amount()
    element.amount_displayed = amount
    element.surface:clear()
    element.numbers_surface:set_text(amount)
    element.background:draw(element.surface)
    element.numbers_surface:draw(element.surface, 2, 16)
  end

  function element:on_draw(dst_surface)
    local x, y = element.dst_x, element.dst_y
    local width, height = dst_surface:get_size()
    if x < 0 then
      x = width + x
    end
    if y < 0 then
      y = height + y
    end
    element.surface:draw(dst_surface, x, y)
  end

  function element:on_started()
    element:check()
    element:rebuild_surface()
  end

  return element
end

return builder