local builder_builder = require"scripts/hud/bar_builder_builder"

local builder = builder_builder:new(game, {
  max_amount_function = function() return sol.main.get_game():get_value"max_stamina" end,
  current_amount_function = function() return sol.main.get_game():get_value"stamina" end,
  draw_ratio = 3,
  color = "orange",
  check_frequency = 20,
})

return builder
