-- The money counter shown in the game screen.

local rupees_builder = {}

local rupee_icon_img = sol.surface.create("hud/money_icon.png")

function rupees_builder:new(game, config)

  local rupees = {}

  local digits_text = sol.text_surface.create({
    font = "enter_command",
    font_size = 16,
    horizontal_alignment = "left",
    vertical_alignment = "top",
  })
  local money_displayed = game:get_money()

  local dst_x, dst_y = config.x, config.y

  function rupees:on_draw(dst_surface)

    local x, y = dst_x, dst_y
    local width, height = dst_surface:get_size()
    if x < 0 then
      x = width + x
    end
    if y < 0 then
      y = height + y
    end

    rupee_icon_img:draw(dst_surface, x, y)
    digits_text:draw(dst_surface, x + 13, y)
  end


  --don't show all the time
  function rupees:hide()
    rupees.is_hidden = true
    local fade_speed = 70
    rupee_icon_img:fade_out(fade_speed)
    digits_text:fade_out(fade_speed)
  end
  function rupees:show()
    rupees.is_hidden = false
    rupee_icon_img:fade_in()
    digits_text:fade_in()
  end


  -- Checks whether the view displays correct information
  -- and updates it if necessary.
  local function check()

    local need_rebuild = false
    local money = game:get_money()
    local max_money = game:get_max_money()

    if game.force_money_display and rupees.is_hidden then
      rupees:show()
    end

    -- Current money.
    if money ~= money_displayed then
      if rupees.is_hidden then
        rupees:show()
      end
      need_rebuild = true
      local step_amount
      if math.abs(money_displayed - money) > 10 then
        step_amount = 10
      else
        step_amount = 1
      end
      if money_displayed < money then
        money_displayed = money_displayed + step_amount
      else
        money_displayed = money_displayed - step_amount
      end

      if money_displayed == money  -- The final value was just reached.
          or money_displayed % 3 == 0 then  -- Otherwise, play sound "rupee_counter_end" every 3 values.
        --sol.audio.play_sound("rupee_counter_end")
      end

    else --don't need to rebuild
      if not rupees.is_hidden and not game.force_money_display then
        rupees:hide()
      end
    end

    if digits_text:get_text() == "" then
      need_rebuild = true
    end

    -- Update the text if something has changed.
    if need_rebuild then
      digits_text:set_text(string.format("%03d", money_displayed))
    end

    return true  -- Repeat the timer.
  end

  -- Periodically check.
  check()
  sol.timer.start(game, 30, check)

  return rupees
end

return rupees_builder

