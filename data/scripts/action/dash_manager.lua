local dash_manager = {}

local dash_distances = {80, 96}
local dash_speeds = {300, 400}
local dash_animations = {"roll", "dash"}
local dash_sounds = {"roll_2", "roll_2"}
local invincibility_lengths = {500, 900}
local minimum_distance = 32 --after this distance, the dash will stop before you hit water, a hole, etc

local movement_id = 1
local current_movement


local dash_state = sol.state.create("dashing")
dash_state:set_can_control_direction(false)
dash_state:set_can_control_movement(false)
dash_state:set_can_traverse_ground("hole", true)
dash_state:set_can_traverse_ground("deep_water", true)
dash_state:set_can_traverse_ground("lava", true)
dash_state:set_affected_by_ground("hole", false)
dash_state:set_affected_by_ground("deep_water", false)
dash_state:set_affected_by_ground("lava", false)
dash_state:set_gravity_enabled(false)
dash_state:set_can_come_from_bad_ground(false)
dash_state:set_can_be_hurt(false)
dash_state:set_can_use_sword(false)
dash_state:set_can_use_item(false)
dash_state:set_can_interact(false)
dash_state:set_can_grab(false)
dash_state:set_can_push(false)
dash_state:set_can_pick_treasure(true)
dash_state:set_can_use_teletransporter(true)
dash_state:set_can_use_switch(true)
dash_state:set_can_use_stream(false)
dash_state:set_can_use_stairs(true)
dash_state:set_can_use_jumper(true)
dash_state:set_carried_object_action("throw")

function dash_state:on_started()
end


function dash_manager:dash(game)
    local hero = game:get_hero()
    local intended_direction8 = (game:get_commands_direction() or hero:get_direction() * 2)
    local intended_angle = intended_direction8 * math.pi / 4

    --OPTIONAL: if there are dash upgrades, set dash level. These correspond to values in dash_distances and dash_speeds tables
    local dash_level = 1

    --check to see if there's room to dash
    local obstacle_test_distance = 8
    local is_obstacle = hero:test_obstacles(obstacle_test_distance * math.cos(intended_angle), obstacle_test_distance * math.sin(intended_angle) * -1 )
    if is_obstacle then return end

    --Start state
    dash_state:set_can_be_hurt(false)
    hero:start_state(dash_state)
    sol.timer.start(dash_state, invincibility_lengths[dash_level], function()
      dash_state:set_can_be_hurt(true)
    end)

    --Animation/Sound
    hero:set_animation(dash_animations[dash_level], function()
      hero:set_animation(walking)
    end)
    sol.audio.play_sound(dash_sounds[dash_level])

    --create little dust effects
    local x, y, z = hero:get_position()
    local map = hero:get_map()
    local num_clouds = 3
    local cloud_delay = 50
    for i = 0, num_clouds - 1 do
      sol.timer.start(map, cloud_delay * i, function()
        local hx, hy, hz = hero:get_position()
        map:create_custom_entity({
          direction = 0, x = hx, y = hy, layer = hz, width = 16, height = 16,
          sprite = "entities/dust_cloud_roll", model = "ephemeral_effect"
        })
      end)
    end

    --create movement
    hero:set_direction(intended_direction8 / 2)
    local m = sol.movement.create("straight")
    m:set_angle(intended_angle)
    m:set_speed(dash_speeds[dash_level])
    m:set_max_distance(dash_distances[dash_level])
    m:set_smooth(true)
    m:start(hero, function()
        hero:unfreeze()
        hero.dashing = false
    end)
    function m:on_obstacle_reached()
      hero:unfreeze()
      hero.dashing = false
    end

    --Stop short if yuo're about to go into a hole
    sol.timer.start(hero, minimum_distance / dash_speeds[dash_level] * 1000, function()
      local x, y, z = hero:get_position()
      local ground = map:get_ground(x, y, z)
      --return if you're already over dangerous ground
      if (ground == "hole") or (ground == "deep_water") or (ground == "lava") or (ground == "prickles") then

      else
        function m:on_position_changed()
          local x, y, z = hero:get_position()
          local angle = m:get_angle()
          local tx, ty = x + math.cos(angle), y + math.sin(angle)
          local ground = map:get_ground(tx, ty, z)
          if (ground == "hole") or (ground == "deep_water") or (ground == "lava") or (ground == "prickles") then
            m:stop()
            sol.timer.start(hero, 100, function()
              hero:unfreeze()
            end)
          end
        end
      end
    end)
end

return dash_manager