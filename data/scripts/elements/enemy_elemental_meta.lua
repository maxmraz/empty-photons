local enemy_meta = sol.main.get_metatable"enemy"

--Fire Reactions:
function enemy_meta:react_to_fire(fire)
  local enemy = self
  if enemy.flammible and not enemy.burning then
    enemy:burn()
  elseif not enemy.fire_immunity then
    enemy:hurt(1)
  end
end

function enemy_meta:burn(duration)
  local enemy = self
  local map = enemy:get_map()
  if not duration then duration = 3000 end

  enemy:get_sprite():set_color_modulation{255,50,0}
  local smolder_sprite = enemy:create_sprite("status_effects/enemy_burning", "burning_effect")
  
  enemy.burning = true
  sol.timer.start(map, duration, function()
    enemy.burning = false
    enemy:get_sprite():set_color_modulation{255,255,255}
    if enemy:get_sprite"burning_effect" then
      enemy:remove_sprite(smolder_sprite)
    end
  end)
  sol.timer.start(map, 1000, function()
    if enemy.burning and enemy:exists() then
      enemy:hurt(1)
      return true
    end
  end)
end


--Ice Reactions:
function enemy_meta:react_to_ice()
  local enemy = self
  if not enemy.ice_immunity then
    enemy:hurt(1)
    if not enemy.frozen
 then enemy:freeze() end
  end
end

function enemy_meta:freeze()
  local enemy = self
  local map = enemy:get_map()
  local sprite = enemy:get_sprite()
  local freeze_duration = enemy:get_property("freeze_duration") or 4000
  enemy.frozen = true
  sprite:set_color_modulation{100,200,255}
  enemy:immobilize(freeze_duration)
  if sprite:has_animation"frozen" then sprite:set_animation"frozen" end
  sol.timer.start(map, freeze_duration, function()
    enemy.frozen = false
    sprite:set_color_modulation{255,255,255}
  end)
end


--Lightning

--Fire Reactions:
function enemy_meta:react_to_lightning(lightning)
  local enemy = self
  if not enemy.lightning_immunity then
    enemy:hurt(1)
  end
end

function enemy_meta:react_to_lightning_bolt(lightning)
  local enemy = self
  if not enemy.lightning_immunity then
    enemy:hurt(10)
    enemy:react_to_fire(lightning)
  end
end
