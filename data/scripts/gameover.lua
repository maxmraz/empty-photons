local black = {}
local black_surface = sol.surface.create()
black_surface:fill_color{0,0,0}

function black:on_started()
  black_surface:set_opacity(0)
  black_surface:fade_in(2)
end

function black:on_draw(dst)
  black_surface:draw(dst)
end

local game_meta = sol.main.get_metatable"game"

game_meta:register_event("on_started", function(self)
  local game = self
  local hero = game:get_hero()

  --if there's been a checkpoint set
  if game:get_value"checkpoint_map" then
    if not sol.menu.is_started(black) then
      sol.menu.start(game, black)
    end
    local checkpoint_map = game:get_value"checkpoint_map"
    game:set_suspended(true)
    sol.timer.start(sol.main, 10, function()
      hero:teleport(checkpoint_map, "_same", "immediate")
      local x, y, z = game:get_value"checkpoint_x", game:get_value"checkpoint_y", game:get_value"checkpoint_z"
      --print("Loading hero at:", x, y, z) 
      hero:set_position(x, y, z)
      hero:set_direction(3)
      game:set_life(game:get_max_life())
      game:set_magic(game:get_max_magic())
      sol.timer.start(game, 1000, function()
        game:set_suspended(false)
        sol.menu.stop(black)
      end)
    end)
  else
    sol.menu.stop(black)
  end
end)


game_meta:register_event("on_game_over_started", function(self)
  local game = self
  local map = game:get_map()
  local hero = game:get_hero()
  local x, y, z = hero:get_position()

  hero:set_visible(false)
  sol.audio.stop_music()
  local cam_surface = map:get_camera():get_surface()
  local dark_steps = 1
  local darkDelta = 3
  sol.timer.start(sol.main, 40, function()
    local rgb = cam_surface:get_color_modulation()
    cam_surface:set_color_modulation({math.max(rgb[1]-darkDelta, 0), math.max(rgb[2]-darkDelta, 0), math.max(rgb[3]-darkDelta, 0)})
    dark_steps = dark_steps + 1
    if dark_steps <= 20 then
      return true
    end
  end)

  local sprite_id = hero:get_sprite():get_animation_set()
  local dummy = map:create_custom_entity({
    x=x, y=y, layer=z, direction=0, width=16,height=16,
    sprite = sprite_id,
  })
  local sprite = dummy:get_sprite()
  sprite:set_ignore_suspend()
  sprite:set_animation("dying", function()
    sprite:set_animation"dead"
    sol.timer.start(game, 1000, function()
      sol.menu.start(sol.main, black)
      game:start()
    end)
  end)
end)
