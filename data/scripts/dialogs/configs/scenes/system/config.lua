background = { -- background information
  image = {
    path = "px.png",
  },
}

dialog_box = { -- contains ALL dialog box info (including name box)
  image = {
    position = "bottom", -- The position the dialog box should display at. (can also be a table containing xy values)
    path = "dialogs/dialog_box.png", -- path to dialog box image (leave empty for no image.)
  },
  text = { -- text inside the dialog box
    max_displayed_lines = 3,
    y_offset = 40,
    x_offset = 32,
    line = { -- options for the indvidual lines.
      font = "enter_command", -- a font is required! 8_bit is the default one in new Solarus projects.
      font_size = 16,
    },
    question = {
      line_buffer = 4,
      question_marker = "$?",
      cursor_wrap = true,
      cursor = {
        image = {
          path = "menus/cursor",
          sprite = {
            "walking"
          },
          x_offset = 8,
        }
      }
    }
  },
  name_box = {
    image = {
      path = "",
      position = "topleft",
    },
    line = {
      font = "enter_command",
      font_size = 16,
      color = {130,140,150},
      x_offset = 32,
      y_offset = 22,
    },
  },
}
