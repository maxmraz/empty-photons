-- Manages the characters displayed on screen.

-- Usage:
-- local characters = require("scripts/dialogs/characters/characters")

local PATH = ...
local TOP_DIR = PATH:match"^.+/" or ""

local characters_wrapper = {}

-- Creates the characters_wrapper table
--
-- Returns character table
function characters_wrapper:create()
  local characters = {
    list = {}, -- list of character class objects
  }

  -- Public: Draw character sprites on destination surface
  -- This is called by Solarus. DO NOT CALL MANUALLY!
  --
  -- dst_surface - Surface the characters will be drawn on
  --
  -- Example
  --   on_draw(sol.surface)
  --
  -- Returns nothing
  function characters:on_draw(dst_surface)
    for _, character in pairs(characters.list) do
      if character.sprite ~= nil and character.sprite['draw'] ~= nil then
        character.sprite:draw(dst_surface)
      end
    end
  end

  -- sets up characters in scene
  --
  -- character_configs - table of ALL character configs in scene
  --
  -- Example
  --   set_characters({Link = {...}, Zelda = {...}, Ganondorf = {...}})
  --
  -- Returns nothing
  function characters:set_characters(character_configs)
    characters.list = {}

    for name, attributes in pairs(character_configs) do
      characters.list[name] = require(TOP_DIR.."character/character"):create(name, attributes)
    end
  end

  -- Public: Update the displayed characters
  --
  -- current_speaker - table of info about the speaking character (e.g. name, emotion)
  -- dialog_box_grahic - A sol.surface or sol.sprite object
  --
  --  Example:
  --    set_character({name = 'Tom', emotion = 'stoic'}, sol.surface.new())
  --
  --  Returns Nothing
  function characters:update(current_speaker, dialog_box_grahic)
    for _, character in pairs(characters.list) do character:update(current_speaker, dialog_box_grahic) end
  end

  -- Public: transitions character
  --
  -- state A string containing either 'enter' or 'exit'
  --
  -- Example:
  --
  --   transition('enter')
  --
  --   transition('exit')
  --
  -- Returns nothing
  function characters:transition(state)
    for _, character in pairs(characters.list) do character:transition(state) end
  end

  return characters
end

return characters_wrapper
