-- A wrapper around individual characters
--
-- Usage:
-- local character = require("scripts/dialogs/characters/character/character")

local PATH = ...
local TOP_DIR = PATH:match"^.+/" or ""
local VNS_DIR = TOP_DIR:match"^(.+/)[^/]+/[^/]+/" --up two directories

local display_position = require(VNS_DIR.."libs/display_position")
local image_helper = require(VNS_DIR.."libs/image_helper")
local transition_manager = require(VNS_DIR.."libs/transitions")

local character_wrapper = {}

-- Using the passed in image information sets the character's xy
--
-- sprite - A sol.image or sol.sprite object
-- image_config - A table of image information
-- dialog_box_graphic - A sol.surface of the dialog box image
--
-- Example:
--   set_character_position(sol.sprite, { path = 'my/path', position = "right", ...}, sol.surface)
--
-- Returns nothing
local function set_character_position(sprite, image_config, dialog_box_grahic)
  local window = nil
  if image_config ~= nil and image_config.relative_to_dialog_box == true then window = dialog_box_grahic end
  local x, y = display_position:compute_position(sprite, image_config.position, window)

  if image_config.x_offset ~= nil then x = x + image_config.x_offset end
  if image_config.y_offset ~= nil then y = y + image_config.y_offset end
  sprite:set_xy(x, y)
end

-- Creates a new character class
--
-- name - A string containing the character name
-- attributes - A table of character information
--
-- Example
--   character_wrapper:create("John", {image = {...}, emotions = {...}, ...})
--     #=> {...}
--
-- Returns new character table.
function character_wrapper:create(name, attributes)
  local character = {
    name = name,
    attributes = attributes,
    current_image_config = attributes.image or {},
    sprite = nil
  }

  -- Update character
  --
  -- current_speaker_info -  table of information about the current_speaker
  -- dialog_box_graphic - A sol.surface of the dialog box image
  --
  -- Example
  --   update({image = {...}, emotions = {...}, sol.surface)
  --
  -- Returns nothing
  function character:update(current_speaker, dialog_box_grahic)
    if current_speaker.name == name then
      local emotions = character.attributes.emotions or {}
      local emotion = current_speaker.emotion
      if type(emotion) == "string" and emotions ~= nil and emotions[emotion] ~= nil then
        character.current_image_config = emotions[emotion]['image']
      end
    end

    character.sprite = image_helper:get_image(character.current_image_config)

    if character.sprite ~= nil then
      set_character_position(character.sprite, character.current_image_config, dialog_box_grahic)
    end
  end

  -- Handles character transition on and off screen
  --
  -- state - A string which contains either "enter" or "exit"
  --
  -- Example
  --   transition('enter')
  --
  --   transition('exit')
  --
  -- Returns nothing
  function character:transition(state)
    if character.sprite ~= nil and character.attributes.transitions ~= nil then
      transition_manager:transition(state, character.sprite, character.attributes.transitions)
    end
  end

  return character
end

return character_wrapper
