require("scripts/multi_events")

local menu = {}

local MENU_DIRECTIONS = {
	prev_menu = "left",
	next_menu = "right",
}


function menu:initialize(game)
  local dash_manager = require"scripts/action/dash_manager"
  local button_mapping_menu = require"scripts/menus/button_mapping"
  local command_manager = require"scripts/misc/command_binding_manager"

  local function next_submenu() print("Submenu swap not yet implemented") end


  local function on_command_pressed(command)
    local hero = game:get_hero()
    local map = game:get_map()
    local handled = false

    local submenu_index = command and command:match"^menu_(%d+)$"
    if submenu_index and not game:is_dialog_enabled() and not sol.menu.is_started(button_menu) then
        --open (or close if already open) the corresponding pause submenu directly
        pause_menu:toggle_submenu(submenu_index)
        return true
    elseif command == "prev_menu" and sol.menu.is_started(pause_menu) then
      next_submenu"left"
    elseif command == "next_menu" and sol.menu.is_started(pause_menu) then
      next_submenu"right"

    elseif command == "action" and game:get_command_effect"action" == nil then
      game:queue_command_until_free"action"
      handled = true

    elseif command == "attack" then
      game:queue_command_until_free"attack"
      handled = true

    elseif command == "grenade" then
      if hero:get_state() == "free" and game:has_item("weapons/grenade") then
        game:get_item("weapons/grenade"):on_using()
        handled = true
      end

    elseif command == "fire" then
      --hack because items don't recognize custom commands"
      local state, state_ob = hero:get_state()
      if (state == "custom") and (state_ob:get_description() == "aiming") then
        state_ob:on_command_pressed("fire")
      end

    elseif command == "switch_gun" then
      local guns = {"pistol", "shotgun", "sniper",}
      if not game.current_gun_index then
        game.current_gun_index = 1
      end
      game.current_gun_index = (game.current_gun_index) % #guns + 1
      local new_gun = guns[game.current_gun_index]
      --keep looping if you don't have that gun yet
      while not game:get_value("possession_" .. new_gun) do
        game.current_gun_index = (game.current_gun_index) % #guns + 1
        new_gun = guns[game.current_gun_index]
      end
      game:set_value("equipped_gun", new_gun)
      sol.audio.play_sound"gun_switch"


    end
    return handled
  end


  function game:on_command_released(command)

  end


  function game:on_joypad_button_pressed(button)
    local command = command_manager:get_command_from_button(button)
    on_command_pressed(command)
  end

  function game:on_joypad_button_released(button)
    local command = command_manager:get_command_from_button(button)
    game:on_command_released(command)
  end


  function game:on_joypad_axis_moved(axis, state)
    --Watch out, axis are fucken tricky
    local command = command_manager:get_command_from_axis(axis, state)
    on_command_pressed(command)
  end


  function game:on_joypad_hat_moved(hat, direction8)
    local command = command_manager:get_command_from_hat(hat, direction8)
  end


  game:register_event("on_key_pressed", function(self, key, modifiers)
    local hero = game:get_hero()
    local command = command_manager:get_command_from_key(key)
    on_command_pressed(command)

    if key == "escape" then
      game:simulate_command_pressed"pause"

    elseif key == "f1" and not game:is_dialog_enabled() and not game:is_paused() then
      if not sol.menu.is_started(button_mapping_menu) then
        sol.menu.start(game, button_mapping_menu)
      else
        sol.menu.stop(button_mapping_menu)
      end

    end

  end)


  game:register_event("on_key_released", function(self, key)
    local command = command_manager:get_command_from_key(key)
    game:on_command_released(command)
  end)


  function game:set_controller_type(type)
    local mapping = require("scripts/joypad_defaults/" .. type)
    for button, command in pairs(mapping.button) do
      game:set_command_joypad_binding(command, "button " .. button)
    end
  end




  function game:queue_command_until_free(command)
    local QUEUE_LIFESPAN = 300 --length in which a command can sit in the queue
    local hero = game:get_hero()
    local hero_state, state_object = hero:get_state()
    if hero:get_state() == "free" then
      game:do_queued_command(command)
    elseif command == "attack" and state_object and state_object:get_description() == "aiming" then
      --hack to prevent attack from queuing when pressing the "fire" button for the gun weapon
    else
      if game.queued_command_timer then game.queued_command_timer:stop() end
      local queue_lifetime = 0
      game.queued_command_timer = sol.timer.start(game, 10, function()
        queue_lifetime = queue_lifetime + 10
        if hero:get_state() == "free" then
          game:do_queued_command(command)
        elseif queue_lifetime < QUEUE_LIFESPAN then
          return true
        end
      end)
    end
  end


  local can_dash = true
  function game:do_queued_command(command)
    local hero = game:get_hero()
    local intended_direction = game:get_commands_direction() or hero:get_direction()
    if command == "action" and hero:get_controlling_stream() == nil
    and game:get_command_effect"action" == nil then
      if hero:get_facing_entity() and hero:get_facing_entity().on_interaction then return end
      if not game:is_suspended() and can_dash then
        dash_manager:dash(game)
        can_dash = false
        sol.timer.start(game, 500, function() can_dash = true end)
      end
      handled = true

    elseif command == "attack" then
      hero:set_direction(game:get_direction_held() / 2)
      local weapon = game:get_item(game:get_value("equipped_weapon"))
      weapon:on_using()

    end
  end

  
end


return menu