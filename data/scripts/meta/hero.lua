-- Initialize hero behavior specific to this quest.

require("scripts/multi_events")

local hero_meta = sol.main.get_metatable("hero")

hero_meta:register_event("on_created", function(self)
  local hero = self
  hero:set_walking_speed(90)
end)


function hero_meta:on_taking_damage(damage)
  local hero = self
  local game = self:get_game()
  local defense = game:get_value("defense") or 4
  damage = math.floor(damage*4 / defense)
  if game.take_half_damage then
    damage = damage / 2
  end
  if damage < 1 then
    damage = 1
  end

  --if this attack would kill you in 1 hit at above 40% max life
  if damage >= game:get_life()
  and game:get_life() >= game:get_max_life() * .4
  and damage >= game:get_max_life() * .6
  and not game.guts_save_used then
    --leave you with half a heart
    damage = game:get_life() - 1
    game:get_map():get_camera():shake()
    sol.audio.play_sound"ohko"
    --set this mechanic on a cooldown
    game.guts_save_used = true
    sol.timer.start(game, 40 * 1000, function() game.guts_save_used = false end)
  elseif damage >= game:get_max_life() * .5 then
    sol.audio.play_sound"oh_lotsa_damage"
  end

  game:remove_life(damage)
  self:get_map():get_camera():shake({count = 4, amplitude = 5, speed = 100})

  local iframe_length = 1000
  hero:set_invincible(true, iframe_length)
  hero:set_blinking(true, iframe_length)
end


hero_meta:register_event("on_state_changed", function(self, state)
  local hero = self
  local game = sol.main.get_game()

  if state == "back to solid ground" then
    hero:set_invincible(true,1500)
  elseif state == "falling" then
    hero:stop_movement()
  elseif state == "hurt" then
  end
end)


hero_meta:register_event("on_state_changing", function(self, old_state, state)

end)


function hero_meta:become_all_powerful()
  local game = self:get_game()
  game:set_value("sword_damage", 25)
  game:set_value("bow_damage", 25)
  game:set_value("defense", 25)
  game:set_max_life(52)
  game:set_life(52)
end

local MAX_BUFFER_SIZE = 48
function hero_meta:on_position_changed(x,y,z)
  local hero = self
  if not hero.position_buffer then hero.position_buffer = {} end
  local hero = self
  local dir = hero:get_sprite():get_direction()
  table.insert(hero.position_buffer, 1, {x=x,y=y,layer=l,direction=dir})

  if #hero.position_buffer > MAX_BUFFER_SIZE then
    table.remove(hero.position_buffer)
  end
end


function hero_meta:get_can_be_hurt()
  local hero = self
  local can_be_hurt = not hero:is_invincible()
  local state, state_ob = hero:get_state()
  if (state == "falling") or (state == "stairs") or (state == "treasure") or (state == "victory") then
    can_be_hurt = false
  elseif state == "custom" then
    if state_ob:get_description() == "feather_jumping" then can_be_hurt = false end
    can_be_hurt = state_ob:get_can_be_hurt()
  end
  return can_be_hurt
end


function hero_meta:ragdoll(direction, distance)
  local hero = self
  local state = sol.state.create("ragdolling")
  state:set_can_control_direction(false)
  state:set_can_control_movement(false)
  state:set_can_traverse_ground("hole", true)
  state:set_can_traverse_ground("deep_water", true)
  state:set_can_traverse_ground("lava", true)
  state:set_affected_by_ground("hole", false)
  state:set_affected_by_ground("deep_water", false)
  state:set_affected_by_ground("lava", false)
  state:set_gravity_enabled(false)
  state:set_can_come_from_bad_ground(false)
  state:set_can_be_hurt(false)
  state:set_can_use_sword(false)
  state:set_can_use_item(false)
  state:set_can_interact(false)
  state:set_can_grab(false)
  state:set_can_push(false)
  state:set_can_pick_treasure(false)
  state:set_can_use_teletransporter(false)
  state:set_can_use_switch(false)
  state:set_can_use_stream(false)
  state:set_can_use_stairs(false)
  state:set_can_use_jumper(false)
  state:set_carried_object_action("throw")
  hero:start_state(state)
  hero:set_animation"ragdolling"
  local m = sol.movement.create"straight"
  m:set_speed(200)
  m:set_angle(direction)
  m:set_max_distance(distance)
  m:start(hero, function() hero:start_knock_down() end)
  function m:on_obstacle_reached()
    hero:start_knock_down()
  end
end


function hero_meta:start_knock_down(duration)
  local hero = self
  local game = self:get_game()
  duration = duration or 700
  local state = sol.state.create("knocked_down")
  state:set_can_control_direction(false)
  state:set_can_control_movement(false)
  state:set_can_come_from_bad_ground(false)
  state:set_can_be_hurt(false)
  state:set_can_use_sword(false)
  state:set_can_use_item(false)
  state:set_can_interact(false)
  state:set_can_grab(false)
  state:set_can_push(false)
  state:set_can_pick_treasure(false)
  state:set_can_use_teletransporter(false)
  state:set_can_use_switch(false)
  state:set_can_use_stream(false)
  state:set_can_use_stairs(false)
  state:set_can_use_jumper(false)
  state:set_carried_object_action("throw")
  hero:start_state(state)
  hero:set_animation"dead"
  sol.timer.start(hero, duration, function()
    if not game.playing_knock_down_getup_sound then
      game.playing_knock_down_getup_sound = true
      sol.audio.play_sound"roll_2"
      sol.timer.start(game, 100, function() game.playing_knock_down_getup_sound = false end)
    end
    hero:unfreeze()
  end)
end


return true