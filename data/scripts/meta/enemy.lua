local enemy_meta = sol.main.get_metatable"enemy"

enemy_meta:register_event("on_hurt", function(self, attack)
  local enemy = self
  local game = enemy:get_game()
  if attack == "explosion" then
    enemy:remove_life(game:get_value("explosion_damage") or 10)
  end
end)


---Make coins when dying
enemy_meta:register_event("on_dying", function(self)
  local enemy = self
  local map = enemy:get_map()
  local exp = enemy.exp or 5
  if exp == 0 then return end
  local dropped_exp = math.max(0, math.random(exp - 10, exp + 10)) / 5
  local x, y, z = enemy:get_position()
  for i = 1, dropped_exp do
    local coin = map:create_pickable{
      x=x, y=y, layer=z,
      treasure_name = "pickables/coin",
      treasure_variant = 2,
    }
    local m = sol.movement.create"straight"
    m:set_angle(math.rad(math.random(0, 360)))
    m:set_max_distance(math.random(4, 32))
    m:set_speed(200)
    m:start(coin)
    function m:on_position_changed()
      m:set_speed(m:get_speed() - 5)
    end
  end
end)


--Count total robots killed:
enemy_meta:register_event("on_dead", function(self)
  local enemy = self
  local game = self:get_game()
  if enemy:get_breed():match("bot") then
    local bots_killed = game:get_value("robot_enemies_killed") or 0
    game:set_value("robot_enemies_killed", bots_killed + 1)
  end
end)


function enemy_meta:immobilize(duration)
  local enemy = self
  local sprite = enemy:get_sprite()
  local map = enemy:get_map()

  sol.timer.stop_all(enemy)
  enemy:stop_movement()
  if sprite:has_animation"immobilized" then
    sprite:set_animation"immobilized"
  elseif sprite:has_animation"stopped" then
    sprite:set_animation"stopped"
  elseif sprite:has_animation"walking" then
    sprite:set_animation"walking"
  end

  if enemy.on_immobilized then enemy:on_immobilized() end

  sol.timer.start(map, duration or 3000, function()
    enemy:restart()
  end)
end


function enemy_meta:is_on_screen()
  local enemy = self
  local map = enemy:get_map()
  local camera = map:get_camera()
  local camx, camy = camera:get_position()
  local camwi, camhi = camera:get_size()
  local enemyx, enemyy = enemy:get_position()

  local on_screen = enemyx >= camx and enemyx <= (camx + camwi) and enemyy >= camy and enemyy <= (camy + camhi)
  return on_screen
end


function enemy_meta:set_has_shadow(has_shadow, size)
  local enemy = self
  if not has_shadow then has_shadow = true end
  if not size then size = "medium" end
  assert(type(has_shadow) == "boolean", "enemy:set_has_shadow() - argument 1 must be a boolean" )
  if has_shadow then
    local sprite = enemy:create_sprite("shadows/shadow_" .. size, "shadow")
    sprite:set_direction(0)
    enemy:bring_sprite_to_back(sprite)
  else
    enemy:remove_sprite(enemy:get_sprite"shadow")
  end
end
