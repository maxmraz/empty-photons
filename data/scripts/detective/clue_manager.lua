--[[
Created by Max Mraz, licensed under the MIT license. Check https://gitlab.com/maxmraz/cybersea/-/wikis/Detective-System for details and explanation.

This is a system for creating a very simple mystery-solving or detective-like experience for the player. When the player interacts with certain CLUES, this system shows a dialog
When all the clues in a set have been found, the system shows a CONCLUSION dialog, and sets a "solved" savegame value.

Usage:
require("scripts/detective/clue_manager") --or wherever you save this script
Create dialogs following the pattern: _clues.mystery_name.clue_name, as well as a _clues.mystery_name.conclusion for each mystery.
Then call game:show_clue({
  mystery= "mystery_name",
  clue= "clue_name",
  sprite = "sprite_you_want_to_show.png"
})
To see if a mystery has been solved, just check game:get_value("detsys_mystery_name_clue_name")
--]]



--CONFIG:
--NOTE: these can be set to adjust where the sprite or png is drawn, relative to the center of the screen
--By default, sprites are drawn with their origin at the center of the screen, and pngs are drawn with their center at the center of the screen.
--You will most likely want to set the png_center_offset_y to some negative value, or else there will probably be overlap with the textbox
local png_center_offset_x = 0
local png_center_offset_y = -32
local sprite_center_offset_x = 0
local sprite_center_offset_y = 0
local completion_sound = "treasure_short" --sound played when all clues have been found



local game_meta = sol.main.get_metatable("game")
local mystery_config = require"scripts/detective/mystery_config" or {}

--Create a table of the dialog IDs starting with "_clues"
local id_table = {}
local lang_reader_env = {
  dialog = function(dialog_table)
    local id = dialog_table.id
    if id:match("_clues.") then
      id_table[id] = id
    end
  end
}
local chunk = sol.main.load_file"languages/en/text/dialogs.dat"
setfenv(chunk, lang_reader_env)
chunk()

--Fill out mystery_config table using the IDs table
for k, id in pairs(id_table) do
  local mystery_name = id:match('_clues.([%w_]+).')
  local clue_name = id:match('_clues.' .. mystery_name .. '.([%w_]+)')
  if not mystery_config[mystery_name] then
    mystery_config[mystery_name] = {}
  end
  if not (clue_name == "conclusion") then
    mystery_config[mystery_name].num_clues = (mystery_config[mystery_name].num_clues and mystery_config[mystery_name].num_clues + 1) or 1
  end
end

for k, v in pairs(mystery_config) do
--  print(k, v.num_clues)
end


function is_solved(mystery_name)
  local game = sol.main.get_game()
  local num_found = game:get_value("detsys_" .. mystery_name .. "_counter") or 0
  if mystery_config[mystery_name].num_clues <= num_found then
    return true
  else
    return false
  end
end


--Sprite display menu--
local display = {}
display.sprite_surface = sol.surface.create()

function display:on_draw(dst)
  display.sprite_surface:draw(dst)
end

function display:set_sprite(mystery_name, sprite_name)
  display.sprite_surface:clear()
  if not sprite_name then return end --don't continue if a sprite was not given
  display.sprite_surface:fill_color({0,0,0,200})
  local sprite
  --Check if sprite_name is a full path, otherwise assume it exists in sprites/clues/mystery_name/sprite_name
  if sol.file.exists("sprites/" .. sprite_name .. ".dat") then
    --print"Full path to sprite given"
    sprite = sol.sprite.create(sprite_name)
  elseif sol.file.exists("sprites/" .. sprite_name ) then
    --print"Full path to png given"
    sprite = sol.surface.create(sprite_name .. ".png")
    sprite.is_png = true
  else
    --print"sprite is relative to sprites/clues/mystery_name"
    if sprite_name:match(".png") then
      sprite = sol.surface.create("clues/" .. mystery_name .. "/" .. sprite_name)
      sprite.is_png = true
    else
      sprite = sol.sprite.create("clues/" .. mystery_name .. "/" .. sprite_name)
    end
  end
  local x, y
  local width, height = display.sprite_surface:get_size()
  if sprite.is_png then
    local sw, sh = sprite:get_size()
    x = width / 2 - sw / 2 + png_center_offset_x
    y = height / 2 - sh / 2 + png_center_offset_y
  else
    x = width / 2 + sprite_center_offset_x
    y = height / 2 + sprite_center_offset_y
  end
  sprite:draw(display.sprite_surface, x, y)
end




function game_meta:show_clue(props)
  local game = self
  local mystery_name = props.mystery
  local clue_name = props.clue
  local sprite  = props.sprite
  local callback = props.callback

  if not mystery_name or not clue_name then
    error("find_clue requires mystery_name and clue_name values")
  end

  --Get the config table
  local mystery = mystery_config[mystery_name]

  --If the mystery is solved and mystery config is set to not show clues after completion, then don't show the clue
  if is_solved(mystery_name) and mystery.hide_clues_after_completion then
    return
  end

  --Show Sprite
  display:set_sprite(mystery_name, sprite)
  sol.menu.start(game, display)

  --Show dialog
  game:start_dialog("_clues." .. mystery_name .. "." .. clue_name, function()
    local counter_variable_name = "detsys_" .. mystery_name .. "_counter"
    local current_count = game:get_value(counter_variable_name) or 0

    --If you've already seen this one, show a dialog and move on
    if game:get_value("detsys_" .. mystery_name .. "_" .. clue_name .. "_found") then
      game:start_dialog("_clues.already_found", function()
        sol.menu.stop(display)
      end)

    else --This is the first time seeing this clue
      --Iterate the mystery counter
      game:set_value("detsys_" .. mystery_name .. "_" .. clue_name .. "_found", true)
      current_count = current_count + 1
      game:set_value(counter_variable_name, current_count)
      --stop showing the sprite
      sol.menu.stop(display)

      --Show conclusion if we've found all the clues
      if current_count >= mystery.num_clues and not game:get_value("detsys_" .. mystery_name .. "_solved") then
        sol.audio.play_sound(completion_sound)
        game:start_dialog("_clues." .. mystery_name .. ".conclusion", function()
          game:set_value("detsys_" .. mystery_name .. "_solved", true) 
          --Conclusion callbacks:
          local map = game:get_map()
          if map.on_mystery_solved then
            map:on_mystery_solved(mystery_name)
          elseif mystery.conclusion_callback then
            mystery_conclusion_callback(map)
          end
        end)
      end

      --Callbacks
      if callback then callback() end
    end

  end)

end


function game_meta:get_detective_mystery_config()
  return mystery_config
end

