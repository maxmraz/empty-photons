--[[
Created by Max Mraz, licensed MIT
Basic options menu
--]]

--Create Options
local options_list = {
  "sound_volume",
  "music_volume",
  "fullscreen",
  "keybind",
  "quit",
}

local allowed_objects = {}
for _, option in pairs(options_list) do
  local object = {}
  object.name = option
  object.text_config = {text_key = "menu.options." .. option}
  object.text_offset = {x = 16, y = 8}
  table.insert(allowed_objects, object)
end

local menu_x, menu_y = 80, 48

local config = {
  origin = {x = menu_x, y = menu_y},
  background_offset = {x = 0, y = -6},
  grid_size = {columns=1, rows=5},
  cell_size = {width=128, height=16},
  cell_spacing = 4,
  edge_spacing = 8,
  background_png = "menus/options_background.png",
  cursor_style = "menus/cursor",
  cursor_offset = {x=5, y=7},
  cursor_sound = "cursor",
}
config.allowed_objects = allowed_objects

local menu = require"scripts/menus/trillium_menu_lib/grid_menu".create(config)

local bind_menu = require("scripts/menus/button_mapping")

local slider_factory = require"scripts/menus/components/slider"
local slider_config = {
  x = 416 / 2 - 108 / 2, y = 96,
  size = {width = 108, height = 16},
  line_length = 100,
  --background_png = "menus/slider_background.png",
}
local sound_slider = slider_factory.create(slider_config)
local music_slider = slider_factory.create(slider_config)


menu:register_event("on_command_pressed", function(self, command)
  local handled = false
  if command == "action" then
    local selection = menu:get_selected_object().name
    menu:process_selection(selection)
    handled = true
  end
  return handled
end)


function menu:process_selection(selection)
  if selection == "sound_volume" then
    sound_slider:set_position(sol.audio.get_sound_volume()) --set the slider's initial position
    function sound_slider:process_change(new_level)
      sol.audio.set_sound_volume(new_level)
      sol.audio.play_sound"cursor"
    end
    sol.menu.start(menu, sound_slider)

  elseif selection == "music_volume" then
    music_slider:set_position(sol.audio.get_music_volume()) --set the slider's initial position
    function music_slider:process_change(new_level)
      sol.audio.set_music_volume(new_level)
      sol.audio.play_sound"cursor"
    end
    sol.menu.start(menu, music_slider)

  elseif selection == "fullscreen" then
    local is_fullscreen = sol.video.is_fullscreen()
    sol.video.set_fullscreen(not is_fullscreen)

  elseif selection == "keybind" then
    sol.menu.start(menu, bind_menu)

  elseif selection == "quit" then
    sol.main.reset()

  end
end


--Make this the pause menu:
--This method means this menu needs to be required in features.lua to do itself
local game_meta = sol.main.get_metatable"game"

function game_meta:on_paused()
  sol.menu.start(self, menu)
end

function game_meta:on_unpaused()
  sol.menu.stop(menu)
end

return menu