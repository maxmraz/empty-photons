local menu = {}

local black_overlay = sol.surface.create()
black_overlay:fill_color{0,0,0}

local view_offset = 0

local background = sol.surface.create("menus/title_screen/background_1.png")

function menu:on_started()
  black_overlay:fade_out()

end


function menu:on_draw(dst)
  background:draw(dst)
  black_overlay:draw(dst)
end


return menu
