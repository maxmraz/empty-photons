local manager = {}

function manager:get_config()
  local config = {
    x = 25,
    y = 140,
    cursor_offset = {x = -3, y = -3},
    cursor_style = "menus/cursor",
    cursor_offset = {x=-2, y=0},
  }

  return config
end

return manager
