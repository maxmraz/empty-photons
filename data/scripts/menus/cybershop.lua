--A configuration file to create a basic grid menu.

--Liscened under MIT. Authored by J. Cournoyer and Max Mraz

local menu = require"scripts/menus/cybershop_template".create{
  grid_size = {c=1, r=6}, --Grid Size is a table comprised of two key/value pairs; the # of Rows and Columns in the grid.
  allowed_objects = { --Allowed Objects is a table that include the names of all the items allowed on this grid menu.
    {
      item = "upgrades/health",
      price = 4,
      string = "shop.health_upgrade",
      availability_function = function(game)
        return game:get_max_life() <= 20 
      end,
    },
    {
      item = "upgrades/magic",
      price = 4,
      string = "shop.magic_upgrade",
      availability_function = function(game)
        return game:get_max_magic() <= 205 
      end,
    },
    {
      item = "upgrades/sword_damage",
      price = 6,
      string = "shop.sword_upgrade",
      availability_function = function(game)
        return game:get_value("solforge_basic_sword_attack_power") <= 6
      end,
    },
    {
      item = "upgrades/ammo_mod_pistol",
      price = 6,
      string = "shop.pistol_upgrade",
      availability_function = function(game)
        local modifier = game:get_value("pistol_ammo_cost_multiplier")
        return not modifier or (modifier >= 60)
      end,
    },
    {
      item = "upgrades/ammo_mod_shotgun",
      price = 6,
      string = "shop.shotgun_upgrade",
      availability_function = function(game)
        local modifier = game:get_value("shotgun_ammo_cost_multiplier")
        return not modifier or (modifier >= 60)
      end,
    },
    {
      item = "upgrades/ammo_mod_sniper",
      price = 6,
      string = "shop.sniper_upgrade",
      availability_function = function(game)
        local modifier = game:get_value("sniper_ammo_cost_multiplier")
        return not modifier or (modifier >= 60)
      end,
    },
  },
  cell_size = {w=256, h=32},  --Cell Size is a table comprised of two key/value pairs; Width and Height of the cell in pixels.
  cell_spacing = 2,  --Cell Spacing is an integer that determines the distance between cells.
  edge_spacing = 4,  --Edge Spacing is an integer that determines the distance between cells and the menu's edge.
}

--Define the controls specific to this menu. These will be added to
--(or replace?) the controls that may be defined in the template script.
menu:register_event("on_command_pressed", function(self, command)
    --Add control behaviour here.
    local handled = false

    return handled
  end)

return menu