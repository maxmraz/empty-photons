-- Create menus that display their contents in a grid.
-- Note: you can put as many objects into the grid as you want, they only scroll vertically though

-- Licensed under MIT. Authored by Cluedrew, J. Cournoyer, and Max Mraz.

local multi_events = require"scripts/multi_events"

local grid_menu = {}

local menu_prototype = {}
multi_events:enable(menu_prototype)

local menu_metatable = {__index = menu_prototype}


--Define behaviour for the startup of every grid menu.
menu_prototype:register_event("on_started", function(self)
  --Pause and stuff
  local game = sol.main.get_game()
  game:set_suspended(true)
  game.force_money_display = true

  local menu = self
  --Add Startup behaviour here.
  --Initialize cursor index (using a base 1 system to match list indexes)
  if not menu.cursor_index then menu.cursor_index = 1 end

  --Create the cursor:
  menu.cursor = sol.sprite.create(menu.cursor_style)

  menu:update_grid()

  menu:update_surface()

end)


menu_prototype:register_event("on_finished", function(self)
  local game = sol.main.get_game()
  game:set_suspended(false)
  game.force_money_display = false
end)


function menu_prototype:update_grid()
  local menu = self
  --Temporary colors for filling surfaces.
  local black = {0,0,0,255}
  local white = {85,80,70,255}

  --menu.view_surface is the surface that will display
  --part of the grid_surface for viewing.
  local view_w, view_h
  view_w = math.ceil(menu.edge_spacing * 2 + menu.cell_spacing * (menu.total_columns - 1) + menu.cell_width * menu.total_columns)
  view_h = math.ceil(menu.edge_spacing * 2 + menu.cell_spacing * (menu.visible_rows - 1) + menu.cell_height * menu.visible_rows)
  menu.view_surface = sol.surface.create(view_w, view_h)

  --menu.grid_surface is the surface that will hold the
  --entire grid of objects.
  local grid_w, grid_h
  grid_w = math.ceil(menu.edge_spacing * 2 + menu.cell_spacing * (menu.total_columns - 1) + menu.cell_width * menu.total_columns)
  grid_h = math.ceil(menu.edge_spacing * 2 + menu.cell_spacing * (menu.total_rows - 1) + menu.cell_height * menu.total_rows)
  menu.grid_surface = sol.surface.create(grid_w, grid_h)
  menu.grid_surface:fill_color(black)

  local cell_x, cell_y = menu.edge_spacing, menu.edge_spacing
  for i=1, menu.total_num_cells do
    --menu.cell_surface is the surface that will be redrawn
    --for each grid cell.
    local cell_surface = sol.surface.create(menu.cell_width, menu.cell_height)
    cell_surface:fill_color(white)
    cell_surface:draw(menu.grid_surface, cell_x, cell_y)
    cell_x = cell_x + menu.cell_width + menu.cell_spacing

    --If i divided by the number of columns has no remainder
    --then the next cell is the start of a new row.
    if i % menu.total_columns == 0 then
      --When the end of a row is drawn, the cell_x should be 
      --reset to only include the edge spacing.
      cell_x = menu.edge_spacing
      cell_y = cell_y + menu.cell_height + menu.cell_spacing
    end
  end

  --CYBERSHOP
  --Fill in items if available:
  local game = sol.main.get_game()
  local shop_level = game:get_value("shop_level") or 1
  menu.available_objects = {}
  local i = 1
  for _, item_config in pairs(menu.allowed_objects) do
    if item_config.availability_function(game) then
      --set sprite
      local sprite = sol.sprite.create("entities/items")
      sprite:set_animation(item_config.item)
      --Calculate price:
      local price_increment = 30
      local price = item_config.price * price_increment + shop_level * price_increment
      item_config.actual_price = price
      local price_surface = sol.text_surface.create{
        font = "enter_command",
        font_size = 16,
        text = price .. " -",
      }
      --Write description
      local description_surface = sol.text_surface.create{
        font = "enter_command",
        font_size = 16,
        text_key = item_config.string
      }
      --Make a surface:
      local item_surface = sol.surface.create(menu.cell_width, menu.cell_height)
      sprite:draw(item_surface, 16, 16)
      price_surface:draw(item_surface, 32, 12)
      description_surface:draw(item_surface, 64, 12)
      local x_coord, y_coord = menu:get_cursor_coordinates(i)
      local draw_x = (x_coord - 1) * menu.cell_width + menu.edge_spacing + (x_coord-1) * menu.cell_spacing
      local draw_y = (y_coord - 1) * menu.cell_height + menu.edge_spacing + (y_coord-1) * menu.cell_spacing + 4
      item_surface:draw(menu.grid_surface, draw_x, draw_y)

      --set index
      item_config.index = i
      menu.available_objects[i] = item_config
      i = i + 1
    end
  end


end



function menu_prototype:update_surface()
  local menu = self
  menu.view_surface:clear()
  menu.grid_surface:draw(menu.view_surface)
  local cursor_coord_x, cursor_coord_y = menu:get_cursor_coordinates()
  cursor_coord_x = (cursor_coord_x - 1) * (menu.cell_width + menu.cell_spacing) + menu.edge_spacing
  cursor_coord_y = (cursor_coord_y - 1) * (menu.cell_height + menu.cell_spacing) + menu.edge_spacing
  menu.cursor:draw(menu.view_surface, cursor_coord_x, cursor_coord_y)
end


function menu_prototype:get_cursor_coordinates(index_to_check)
  local menu = self
  index_to_check = index_to_check or menu.cursor_index
  local coord_x = (index_to_check - 1) % menu.total_columns + 1
  local coord_y = math.floor((index_to_check - 1) / menu.total_columns) + 1
  return coord_x, coord_y
end


function menu_prototype:move_cursor(direction)
  local menu = self
  local new_index
  local coord_x, coord_y = menu:get_cursor_coordinates()
  if direction == "right" and (coord_x == menu.total_columns) then
    new_index = menu.cursor_index - menu.total_columns + 1
  elseif direction == "right" then
    new_index = menu.cursor_index + 1
  elseif direction == "up" then
    new_index = menu.cursor_index - menu.total_columns
  elseif direction == "left" and (coord_x == 1) then
    new_index = menu.cursor_index + menu.total_columns - 1
  elseif direction == "left" then
    new_index = menu.cursor_index - 1
  elseif direction == "down" then
    new_index = menu.cursor_index + menu.total_columns
  end
  if (new_index > 0) and (new_index <= menu.total_num_cells) then
    menu.cursor_index = new_index
  end
  menu:update_surface()
end


--This section will determine what is drawn to the screen
--when your menu is active.
menu_prototype:register_event("on_draw", function(self, screen)
  --Add the visual elements of the menu here.
  self.view_surface:draw(screen, 80, 4)
end)

--Default key/command bindings can be written here to give
--each grid menu a basic control set up, such as moving the cursor.
menu_prototype:register_event("on_command_pressed", function(self, command)
  --Add control behaviour here
  local menu = self
  local handled = false
  if (command == "right") or (command == "up") or (command == "left") or (command == "down") then
    self:move_cursor(command)

  elseif command == "action" then
    handled = true
    local game = sol.main.get_game()
    local item_config = menu.available_objects[menu.cursor_index]
    local price = item_config.actual_price
    if game:get_money() < price then
      game:start_dialog("shop.not_enough_money")
    else
      game:start_dialog("shop.confirm", function(answer)
        if answer == 2 then
          game:remove_money(price)
          game:get_hero():start_treasure(item_config.item)
          local shop_level = game:get_value("shop_level") or 1
          game:set_value("shop_level", shop_level + 1)
          self:update_grid()
          self:update_surface()
        end
      end)
    end

    elseif command == "attack" or (command == "pause") then
      handled = true
      sol.menu.stop(menu)
    end
  return handled
end)

function grid_menu.create(config)
    local menu = {}

    --Optional Paramters are handled first and include defaults
    --in case they are not defined in the config.
    menu.cell_width = config.cell_size.w or 16
    menu.cell_height = config.cell_size.h or 16
    menu.cell_spacing = config.cell_spacing or 2
    menu.edge_spacing = config.edge_spacing or 4
    menu.cursor_style = config.cursor_style or "menus/trillium_menus/cursor"
    menu.allowed_objects = config.allowed_objets or {}

    --Mandatory Parameters are handled last. An error is thrown
    --if the specified parameter is not found.
    menu.total_columns = assert(config.grid_size.c, "Menu is missing parameter for # of Columns")
    menu.visible_rows = assert(config.grid_size.r, "Menu is missing parameter for # of Rows")
    menu.allowed_objects = assert(config.allowed_objects, "Menu is missing table of objects to be displayed.")

    --Derived Paramters are determined after all other parameters.
    menu.total_num_cells = #menu.allowed_objects
    menu.total_rows = menu.total_num_cells / menu.total_columns

    setmetatable(menu, menu_metatable)

    return menu
end


return grid_menu