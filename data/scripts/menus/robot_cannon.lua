local menu = {}

local robot_1 = sol.sprite.create"enemies/cybersea_enemies/bot_octo_model"
local robot_2 = sol.sprite.create"enemies/cybersea_enemies/bot_bruiser_model"
local robot_3 = sol.sprite.create"enemies/cybersea_enemies/bot_turret_model"

local robots = {
  robot_1,
  robot_2,
  robot_3,
}
menu.active_robot = robots[1]


function launch_robot()
  menu.active_robot = robots[math.random(1, #robots)]
  menu.active_robot.visible = true

  local screen_w, screen_h = 416, 240

  local m = sol.movement.create"circle"
  m:set_clockwise()
  m:set_center(screen_w * 1.2, screen_h * 3 + math.random(-50, 0))
  m:set_radius(screen_w * 1.5)
--  m:set_radius(24)
--  m:set_center(32, 32)
  m:set_angle_from_center(math.pi)
  m:set_angular_speed(.7)
  m:set_duration(3000)
  m:start(menu.active_robot)

end



function menu:on_started()
  --rotate robot:
  sol.timer.start(menu, 10, function()
    menu.active_robot:set_rotation(menu.active_robot:get_rotation() - math.rad(4))
    return true
  end)

  sol.timer.start(menu, 10, function()
    sol.audio.play_sound"cannon_fire"
    launch_robot()
    return math.random(3500, 6000)
  end)
end


function menu:on_draw(dst)
  if menu.active_robot.visible then
    menu.active_robot:draw(dst)
  end
end


return menu
