return {
  keyboard = {
    up = "up",
    down = "down",
    left = "left",
    right = "right",
    action = "space",
    attack = "c",
    item_1 = "x",
    item_2 = "v",
    pause = "d",
  },
  joypad = {
    up = "axis 1 -",
    down = "axis 1 +",
    left = "axis 0 -",
    right = "axis 0 +",
    action = "button 0",
    attack = "button 1",
    item_1 = "button 2",
    item_2 = "button 3",
    pause = "button 4",
  },
}