local menu = {}

local map_surface = sol.surface.create"menus/city_map.png"

function menu:on_draw(dst)
  map_surface:draw(dst)
end

function menu:on_command_pressed()
  sol.menu.stop(self)
  return true
end

return menu
