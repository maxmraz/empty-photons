local entity = ...
local game = entity:get_game()
local map = entity:get_map()
local sprite

entity.hp = 3

function entity:on_created()
  if game:get_value(entity:get_property"savegame_value") then entity:remove() end
  sprite = entity:get_sprite()
  entity:set_drawn_in_y_order(true)
  entity:set_traversable_by("hero", false)
  entity:set_size(32,16)
  entity:set_origin(16,13)
end

function entity:react_to_solforge_weapon()
  if entity.hp > 0 then
    entity.hp = entity.hp - 1
    sprite:set_animation("hurt", "stopped")
    sol.audio.play_sound"enemy_hurt"
  else
    entity:shatter()
  end
end


local function create_coin(variant)
  local x, y, z = entity:get_position()
  local coin = map:create_pickable{
    x=x, y=y, layer=z,
    treasure_name = "pickables/coin",
    treasure_variant = variant,
  }
  local m = sol.movement.create"straight"
  m:set_max_distance(math.random(16,32))
  m:set_angle(math.rad(math.random(0, 360)))
  m:set_speed(110)
  m:start(coin)
end


function entity:shatter()
  sol.audio.play_sound"enemy_hurt"
  sprite:set_animation("breaking", function()
    entity:remove()
  end)
  local value = entity:get_property("value") or 50
  local num_fives = math.floor(value / 5)
  local num_ones = value % 5
  for i = 1, num_fives do
    create_coin(2)
  end
  for i = 1, num_ones do
    create_coin(1)
  end
  game:set_value(entity:get_property"savegame_value", true)
  local num_collected = game:get_value("trash_chests_collected") or 0
  game:set_value("trash_chests_collected", num_collected + 1)
end