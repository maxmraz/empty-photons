local entity = ...
local game = entity:get_game()
local map = entity:get_map()
local sprite

function entity:on_created()
  sprite = entity:get_sprite()
  entity:set_drawn_in_y_order(true)
  entity:set_traversable_by("hero", false)
  entity:set_size(32,16)
  entity:set_origin(16,13)
end



function entity:destroy()
  sol.audio.play_sound"enemy_hurt"
  sprite:set_animation("breaking", function()
    entity:remove()
  end)
end
