local entity = ...
local game = entity:get_game()
local map = entity:get_map()

local range = 120

function entity:on_created()
  entity:set_drawn_in_y_order(true)
  entity:set_traversable_by(false)
  entity.powered = false
  if entity:get_property("initially_powered") then entity:turn_on() end
end

function entity:turn_on()
  if entity.powered then return end
  sol.audio.play_sound"switch"
  entity.powered = true
  entity:get_sprite():set_animation"on"
  if not entity.zaps then entity.zaps = {} end
  local x, y, z = entity:get_position()
  for other in map:get_entities_in_rectangle(x - range * 2, y - range * 2, range * 4, range * 4) do
    if (other:get_type() == "custom_entity") and (other:get_model() == entity:get_model()) and (other:get_distance(entity) <= range) and not other.powered then
      if not other.powered then
        sol.timer.start(entity, 200, function() other:turn_on() end)
      end
      local zap = map:create_custom_entity{
        x = x, y = y, layer = z, width = 16, height = 16, direction = 0,
        sprite = "elements/zap",
      }
      zap:set_traversable_by("enemy", function() return enemy.lock_facing == true end)
      local zap_sprite = zap:get_sprite()
      zap_sprite:set_scale(other:get_distance(entity) / 100, 1)
      zap_sprite:set_rotation(entity:get_angle(other))
      zap:add_collision_test("sprite", function(zap, target)
        if target.react_to_lightning then
          if target:get_type() == "enemy" and (target:get_distance(map:get_hero()) >= 420) then return end
          target:react_to_lightning(zap)
        end
      end)
      --track zaps:
      zap.parent_node = entity
      zap.recipient_node = other
      entity.zaps[zap] = zap
      if not other.zaps then other.zaps = {} end
      other.zaps[zap] = zap
    end
  end
end


function entity:turn_off()
  if not entity.powered then return end
  sol.audio.play_sound"switch"
  entity.powered = false
  entity:get_sprite():set_animation"off"
  if entity.zaps then
    for _, zap in pairs(entity.zaps) do
      if zap:exists() then
        zap:get_sprite():set_animation"going_out"
        sol.timer.start(zap, 200, function()
          zap:remove()
        end)
      end
      zap.parent_node.zaps[zap] = nil
      zap.recipient_node.zaps[zap] = nil
      zap.recipient_node:check_for_power()
      zap.parent_node:check_for_power()
    end
  end
  entity.zaps = {}
end


function entity:check_for_power()
  local has_power = false
  if entity.zaps then
    for zap in pairs(entity.zaps) do
      if zap then has_power = true end
    end
  end
  if not has_power then
    entity:turn_off()
  end
end


function entity:toggle()
  if entity.powered then
    entity:turn_off()
  else
    entity:turn_on()
  end
end


function entity:react_to_solforge_weapon()
  entity:toggle()
end

function entity:react_to_hero_projectile()
  entity:toggle()
end

function entity:react_to_enemy_projectile()
  entity:toggle()
end
