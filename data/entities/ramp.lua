local entity = ...
local game = entity:get_game()
local map = entity:get_map()

--NOTE: Assumes vertical stairs going in north direction and must be three tiles tall
--Place it on the lower layer, so its north edge meets the edge of the upper layer

function entity:on_created()
  entity:set_visible(false)

  local hero = map:get_hero()
  local corner_x, corner_y, width, height = entity:get_bounding_box()
  local x, y, layer = entity:get_position()
  local direction = entity:get_direction()

  --create platform above entity
  local invisible_platform = map:create_custom_entity{
    x=x, y=y, layer=layer+1, width=width, height=height, direction=direction,
  }
  invisible_platform:set_modified_ground("ladder")

  --create sensors on entity and invisible platform
  local go_up_sensor = map:create_sensor{
    x=x, y=y + height/3, layer=layer, width=width, height=height/3
  }
  local go_down_sensor = map:create_sensor{
    x=x, y=y + 2 * height/3, layer=layer + 1, width=width, height=height/3
  }

  --make sensors move the hero
  function go_up_sensor:on_activated()
    hero:set_layer(hero:get_layer() + 1)
  end
  function go_down_sensor:on_activated()
    hero:set_layer(hero:get_layer() - 1)
  end

  --create walls
  map:create_wall{
    x = corner_x - 8, y = y, layer = layer + 1,
    width = 8, height = height,
    stops_hero = true,
  }
  map:create_wall{
    x = corner_x + width, y = y, layer = layer + 1,
    width = 8, height = height,
    stops_hero = true,
  }
end
