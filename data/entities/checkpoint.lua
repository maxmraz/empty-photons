local entity = ...
local game = entity:get_game()
local map = entity:get_map()


--Add a game function for setting checkpoints, so you can do it outside of these checkpoint entities as well:
local game_meta = sol.main.get_metatable"game"
function game_meta:save_checkpoint()
  local game = self
  local hero = game:get_hero()
  local x, y, z = hero:get_position()
  game:set_value("checkpoint_map", game:get_map():get_id())
  game:set_value("checkpoint_x", x)
  game:set_value("checkpoint_y", y)
  game:set_value("checkpoint_z", z)
end


function entity:on_created()
  entity:set_traversable_by(false)
  entity:set_drawn_in_y_order(true)
end

function entity:on_interaction()
  entity:save_checkpoint()
  entity:heal_hero()

  game:start_dialog("system.save_question", function(answer)
    if answer == 2 then
      game:save()
    end
    entity:sparkle_effect()
    sol.audio.play_sound"bot_piston"
    sol.audio.play_sound"chest_open"
    sol.audio.play_sound"bird_cute"
--    sol.audio.play_sound"bird_cute"
  end)
end


function entity:save_checkpoint()
  game:save_checkpoint()
end


function entity:heal_hero()
  --if you wanted to, checkpoints could heal the hero and/or replenish some of their items
  game:set_life(game:get_max_life())
  game:set_magic(game:get_max_magic())
end



function entity:sparkle_effect()
  local x, y, z = entity:get_position()
  for i=1, 12 do
    local sparkle = map:create_custom_entity{
      x=x, y=y, layer=z, direction=0, width=8, height=16,
      sprite = "entities/lantern_sparkle",
    }
    sparkle:get_sprite():set_animation("sparkle_" .. math.random(1,2), function()
      sparkle:remove()
    end)
    sparkle:get_sprite():set_ignore_suspend(true)
    sparkle:set_drawn_in_y_order(true)
    local m = sol.movement.create"straight"
    m:set_speed(120)
    m:set_angle(math.random(100) * 2 * math.pi / 100)
    m:set_max_distance(math.random(8, 24))
    m:set_ignore_obstacles(true)
    m:set_ignore_suspend(true)
    m:start(sparkle)
  end
end
