local entity = ...
local game = entity:get_game()
local map = entity:get_map()

function entity:on_created()
  entity.hp = entity:get_property("hp")or 1
  
end


function entity:react_to_solforge_weapon()
  entity.hp = entity.hp - 1
  if entity.hp <= 0 then
    entity:destroy()
  else
    sol.audio.play_sound("enemy_hurt")
  end
end

function entity:react_to_hero_projectile()
  entity:react_to_solforge_weapon()
end


function entity:destroy()
  local sprite = entity:get_sprite()
  sol.audio.play_sound(entity:get_property("sound_effect") or "breaking_glass")
  sprite:set_animation("destroy", function()
    entity:set_enabled(false)
    entity:remove()
  end)
end
