local entity = ...
local game = entity:get_game()
local map = entity:get_map()
local hero = map:get_hero()
local sprite
local movement


function entity:on_created()
  entity.is_projectile = true
  entity:set_drawn_in_y_order(true)
  entity:set_can_traverse(true)
  entity:set_can_traverse("enemy", true)
  entity:set_can_traverse("hero", true)
  entity:set_can_traverse_ground("deep_water", true)
  entity:set_can_traverse_ground("hole", true)
  entity:set_can_traverse_ground("lava", true)
  entity:set_can_traverse_ground("low_wall", true)
end

function entity:shoot(angle)
  local damage = entity.damage or 1
  local type = entity.damage_type or "physical"
  local sprite = entity:get_sprite()
  local is_sniper = (sprite:get_animation_set() == "hero_projectiles/sniper")
  sprite:set_transformation_origin(0, -1)
  sprite:set_xy(0, -14)
  sprite:set_rotation(angle)
  local m = sol.movement.create"straight"
  m:set_speed(entity.speed or 300)
  m:set_angle(angle)
  m:set_max_distance(entity.range or 700)
  m:set_smooth(false)
  if is_sniper then
    m:set_ignore_obstacles(true)
  end
  m:start(entity, function()
    entity:hit_obstacle()
  end)
  function m:on_obstacle_reached()
    entity:hit_obstacle()
  end
  function m:on_changed()
    entity:hit_obstacle()
  end

  entity.collided_entities = {}
  function process_collision(entity, other)
    if entity.collided_entities[other] then return end
    entity.collided_entities[other] = true
    if other:get_type() == "enemy" and (other:get_life() > 0) then
      if not is_sniper then
        entity:clear_collision_tests()
      end
      if other.process_hit then
        other:process_hit(damage * (other.bullet_damage_mod or 1))
      else
        other:hurt(damage)
      end
      if not is_sniper then
        entity:hit_obstacle()
      end
    elseif other:get_type() == "switch" and not other:is_walkable() then
      entity:clear_collision_tests()
      entity:hit_obstacle()
      other:toggle()
    elseif other.react_to_hero_projectile then
      other:react_to_hero_projectile()
    end
  end

  entity:add_collision_test("sprite", function(entity, other)
    process_collision(entity, other)
  end)
  entity:add_collision_test("touching", function(entity, other)
    process_collision(entity, other)
  end)

end


function entity:hit_obstacle()
  entity:stop_movement()
  local pop_sprite = entity:create_sprite("hero_projectiles/pop")
  pop_sprite:set_xy(0, -14)
  entity:remove_sprite()
  pop_sprite:set_animation("killed", function() entity:remove() end)
end

