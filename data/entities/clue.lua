local entity = ...
local game = entity:get_game()
local map = entity:get_map()

local shine_interval = 2000

function entity:on_created()
  if game:get_value("show_clue_shines") then
    sol.timer.start(entity, shine_interval * math.random(.8, 1.2), function()
      if entity.checked then return end
      local px, py = entity:get_position()
      local cx, cy = entity:get_center_position()
      local dx = cx - px
      local dy = cy - py
      local shine = entity:create_sprite("entities/sparkle")
      shine:set_xy(dx + math.random(-8, 8), dy + math.random(-16, 0))
      sol.timer.start(entity, 500, function()
        entity:remove_sprite(shine)
      end)
      return shine_interval * math.random(.8, 1.2)
    end)
  end
end

entity:register_event("on_interaction", function()
  entity.checked = true
end)

